//var outPath = '/agent/.tmp/';
var outPath = '/media/';
var request = require('request');
var fs = require('fs');

function convertVideo(fileName) {
  var outFileName = 'screencast.mp4';

  console.log('convertVideo', fileName, outFileName);

  var ffmpeg = require('fluent-ffmpeg');
  var command =
    ffmpeg(fileName)
      .save(outPath + outFileName);
}

function convert(stream, url) {
  var outFileName = 'audio.wav';
  if (url.indexOf('/media/video/') >=0 ) {
    outFileName = 'video.y4m';
  }

  var ffmpeg = require('fluent-ffmpeg');
  var command =
    //ffmpeg('./sounds/message.mp3')
    ffmpeg(stream)
      .on('end', function () {
        console.log('file has been converted successfully', outPath + outFileName, url);
      })
      .on('error', function (err) {
        console.log('an error happened: ' + err.message);
      })
      // save to file
      .save(outPath + outFileName);
}

function get(url) {
//var url = 'http://localhost:9010/api/test_definitions/media/video/552a4a49ffa10c301daa12b0/message.mp3';
  //var url = 'http://localhost:9010/api/test_definitions/media/video/552a4a49ffa10c301daa12b0/Mandelbrot.mp4';

  var r = request({
    method: 'GET',
    uri: url
  });

  convert(r, url);
}

function copyFile(source, target, cb) {
  var cbCalled = false;

  var rd = fs.createReadStream(source);
  rd.on("error", function(err) {
    done(err);
  });
  var wr = fs.createWriteStream(target);
  wr.on("error", function(err) {
    done(err);
  });
  wr.on("close", function(ex) {
    done();
  });
  rd.pipe(wr);

  function done(err) {
    if (!cbCalled) {
      cb(err);
      cbCalled = true;
    }
  }
}

function copy(filePath) {
  console.log('copy', filePath, filePath.indexOf('Silent.wav'));
  var outFileName = outPath + 'video.y4m';
  if (filePath.indexOf('Silent.wav') >=0) {
    //special case for the default audio file, copy from the video storage to the audio folder
    outFileName = outPath + 'audio.wav';
  }
  copyFile(filePath, outFileName, function(err) {
    if (err) {
      console.error(err);
    }
    else {
      console.log('copies video from:', filePath, outFileName);
    }
  })
}

if (process.argv.length < 3) {
  console.error('ConvertMedia: Missing media url');
  return process.exit(1);
}

console.log('ConvertMedia: version 0.0.4');
process.argv.forEach(function (val, index, array) {
  if (index>=2) {
    console.log(index + ': ' + val);

    if (val.indexOf('/video/') === 0) {
      copy(val);
    }
    else if (val.indexOf('/media/') === 0) {
      convertVideo(val);
    }
    else {
      get(val);
    }
  }
});

