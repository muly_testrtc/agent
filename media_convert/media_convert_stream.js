debug = require('debug')('media_convert');
debug = console.info.bind(console);
debug('media_convert version 0.6.18');

//var outPath = '/agent/.tmp/';
var outPath = '/media/';

function convert(stream) {
  var outFileName = 'audio.wav';

  var ffmpeg = require('fluent-ffmpeg');
  var command =
    //ffmpeg('./sounds/message.mp3')
    ffmpeg(stream)
      .on('end', function () {
        debug('file has been converted successfully', outPath + outFileName);
      })
      .on('error', function (err) {
        debug('an error happened: ' + err.message);
      })
      // save to file
      .save(outPath + outFileName);
}

convert(process.stdin);