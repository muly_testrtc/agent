#!/bin/bash

echo "IMAGE {{image-name}}"

sudo mkdir -m 777 .tmp
sudo mkdir -m 777 $TEST_RESULTS_PATH

export GEOMETRY="$SCREEN_WIDTH""x""$SCREEN_HEIGHT""x""$SCREEN_DEPTH"

function shutdown {
  kill -s SIGTERM $NODE_PID
  wait $NODE_PID
}

selenium_args="-Dwebdriver.firefox.profile=nightwatch"
if [ -n "$RTC_FIREFOX_BINARY" ]
then
  echo "Custom firefox binary is set"
  selenium_args="$selenium_args -Dwebdriver.firefox.bin=$RTC_FIREFOX_BINARY"
fi

echo Run xvfb-run
xvfb-run --server-args="$DISPLAY -screen 0 $GEOMETRY -ac +extension RANDR" \
  java -jar /opt/selenium/selenium-server-standalone.jar $selenium_args &
NODE_PID=$!

trap shutdown SIGTERM SIGINT
wait $NODE_PID