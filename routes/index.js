var fs = require('fs');
var path = require('path');
var express = require('express');
var router = express.Router();
var cp = require('child_process');
var request = require('request');
//var request = require('retry-request');
var isWindows = require('os').platform().indexOf('win') === 0;
var sh = require('sync-exec');
var mkdirp = require('mkdirp');

function getUserHome() {
  var home = process.env.HOME || process.env.HOMEPATH || process.env.USERPROFILE;
  if (home === '/root') {
    home = '/home/seluser';
  }

  return home;
}

var webRTC_dir = (isWindows ? 'C:/Users/Muly' : getUserHome()) + '/Downloads/';
var webRTC_fileName = 'webrtc_internals_dump'; //.txt';


var webRTC_ext = '.txt';
var scriptPath = '.tmp/test.js';

//we allow only one script at a time
var runCount = 0;
var childProcess = {};
var g_stdout = [];
var latestPid = null;

var getCurrentSession = function() {
  return childProcess[latestPid];
};

var serverError = function(res, err) {
  console.error('Server error', err);
  return res.status(500).json(err);
};

  /* GET home page. */

function exec(command) {
  //_exec(command)

  if (sh) {
    var code = sh(command);
    console.log('sh', code, '<-', command);
  }
  else {
    console.log('windows version does not execute shell commands', command);
  }
  return code;
}

router.get('/webrtc-stat-clear', function(req, res) {
  console.log('webrtc-stat-clear:start');

  fs.readdir(webRTC_dir, function (err, list) {
    if (err) return serverError(res, err);

    list.forEach(function (file) {
      console.log('Found file', file);

      if (file.indexOf(webRTC_fileName) == 0) {
        var file = path.join(webRTC_dir, file);
        fs.unlinkSync(file);
        console.log('successfully deleted', file);
      }
    });

    res.status(200).send('webrtc-stat-clear');
    console.log('webrtc-stat-clear:end');
  });
});

var getWebRTCStat = function(req, res) {

  var retrySend = 3;
  console.log('Called getWebRTCStat POST');

  function SentResultToAdminServer(fileName, testRun) {
    var url = testRun.adminServerUrl + '/api/agents/json/' + testRun.adminRunId;
    retrySend--;
    console.log(retrySend, 'SentJson:', url);

    var requestPipe = request({ url: url, method: 'POST', strictSSL: false});

    var pipe = fs
      .createReadStream(fileName)
      .pipe(requestPipe);

    pipe
      .on('error', function(err) {
        console.error(retrySend, 'Error SentJson: ',err);
      })
      .on('response', function (answer) {
        //For /retry-request
        console.log(retrySend, 'response SentJson: ');
      })
      .on('complete', function (answer) {
        console.log(retrySend, 'complete SentJson:', answer.statusCode, answer.statusMessage);

        if ((answer.statusCode < 200 || answer.statusCode >= 400) && retrySend >=0) {
          console.log(retrySend, 'Schedule SentResultToAdminServer to retry: ');
          setTimeout(function () {
            console.log(retrySend, 'Call SentResultToAdminServer to retry');
            SentResultToAdminServer(fileName, testRun);
          }, Math.abs(Math.max(1000, 1000 * (3 - retrySend))));
        } else {
          if (res) res.status(200).send(answer.statusCode === 200 ? 'Data sent to admin successfuly' : 'Failed to send answer to admin server: ' + answer.statusCode + ' - ' + answer.statusMessage);
        }
      })
      .on('end', function(answer) {
        console.log(retrySend, 'End SentJson: pipe results completed', answer);
      });

    /*
    var r = request({ url: url, method: 'POST'});

    r
      .on('error', function(err) {
        console.error('SentJson: ',err);
      })
      .on('response', function (answer) {

        var pipe = fs
          .createReadStream(fileName)
          .pipe(answer.req);

        console.log('response SentJson: ');
      })
      .on('complete', function (answer) {
        //For /retry-request
        console.log('complete SentJson: ');
        if (res) res.status(200).send('Data sent to admin');
      })
      .on('end', function(answer) {
        console.log('SentJson: pipe results completed', answer);
        if (res) res.status(200).send('Data sent to admin');
      });
    */
  }

  var fileCount = 1;
  console.log('get-webrtc-stat:start', fileCount);

  var retry = 10;

  function sendFile(fileName) {
    console.log('get-webrtc-stat:file exists, wait 0.5 sec to write complete', fileName);
    //Wait another 2 seconds to make sure the file was written completely
    setTimeout(function () {
      //Bad place the container will terminate before we finish the send
      //if (res) res.end('done, not returning the dump to save memory');
      var testRun = getCurrentSession();
      if (testRun) {
        SentResultToAdminServer(fileName, testRun);
      }
      else {
        console.error('Unknown adminServerUrl result was not sent to admin');
        if (res) res.status(200).send('Unknown adminServerUrl result was not sent to admin');
      }
    }, 500);
  }

  function waitForFile(fileName) {
    console.log('get-webrtc-stat:wait for file', fileName);
    setTimeout(function () {
      console.log('get-webrtc-stat:inside setTimeout');

      if (fs.existsSync(fileName)) {
        console.log('get-webrtc-stat:inside file found send the file');
        sendFile(fileName);
      }
      else if (retry <= 0) {
        console.error('get-webrtc-stat:file not found');
        if (res) res.status(409).send('failed to find file' + webRTC_fileName);
      }
      else {
        console.error('get-webrtc-stat:file not found, retry', retry);
        retry--;
        waitForFile(fileName);
      }
    }, 1000);
  }

  var browserLogs = req.body;
  console.log('browserLogs', browserLogs);
  var testRun = getCurrentSession();
  if (testRun) {
    testRun.browserLogs = browserLogs;
  }

  var fileName;
  fileName = webRTC_dir + webRTC_fileName + webRTC_ext;
  if (fs.existsSync(fileName)) {
    console.log('file exists send file');
    sendFile(fileName);
  }
  else {
    waitForFile(fileName);
  }
};

router.post('/get-webrtc-stat/', getWebRTCStat);

router.post('/run-test', function(req, res) {
  var sendServerAnswer = function(testRun, code, signal) {
    var url = testRun.adminServerUrl + '/api/agents/end/' + testRun.adminRunId;
    console.log('sendServerAnswer', code, signal, url, testRun.stdout);

    var retrySend = 3;
    var result;
    if (code == 0) {
      result = 'ok';
    }
    else if (code == 130) {
      result = 'terminated';
    }
    else {
      result = 'error('+code+')';
    }

    function send() {
      retrySend--;

      request({
        method: 'POST',
        url: url,
        strictSSL: false,
        body: {
          result: result,
          code: code,
          signal: signal,
          stdout: g_stdout,
          browserLog: testRun.browserLogs
        },
        json: true
      }, function (error, response, body) {
        if (error) {
          console.log(error);
        } else {
          console.log('sendServerAnswer: Answer from server', response.statusCode, response.statusMessage);
        }

        if ((response.statusCode < 200 || response.statusCode >= 400) && retrySend >=0) {
          setTimeout(function () {
            console.log(retrySend, 'Call sendServerAnswer to retry');
            send();
          }, Math.abs(Math.max(1000, 1000 * (3 - retrySend))));
        }
      });
    }

    send();
  };

  var testRun = {
    adminServerUrl: req.body.serverUrl,
    adminRunId: req.body.id,
    runOptions: req.body.runOptions,
    iteartionOptions: req.body.iteartionOptions,
    options: req.body.options,
    process: null,
    stdout: [],
  };

  var env = Object.create( process.env );
  var TEST_RESULTS_PATH = env.TEST_RESULTS_PATH + '/itr_' + testRun.iteartionOptions.runIndex;
  env.TEST_RESULTS_PATH = TEST_RESULTS_PATH;

  console.log('run-test:start', Object.keys(childProcess).length, testRun);
  console.log('webRTC_dir', webRTC_dir);

  var hasVideo = testRun.options.hasVideo;


  //Kill the previus process before start a new one
  //exec('echo `./kill-chrome.sh`');
  for (var pid in childProcess) {
    if (childProcess.hasOwnProperty(pid)) {
      console.log('run-test:kill previous script', pid); // , childProcess);

      var processing = childProcess[pid].process;
      processing.kill('SIGINT');
    }
  }

  /*
  function callNightWatch(args,options) {
    var Nightwatch = require('nightwatch/lib');

    var argv = {
      env: 'chrome',
      test: '.tmp/test.js'
    };

    Nightwatch.runner(argv, function(err, answer, param1) {
      console.log(err, answer, param1);
    }, {});
  }
  */

  function spawnNightWatch(args,options) {
    console.log('spawn', args, options);
    var processing = cp.spawn(isWindows ? 'node' : 'nodejs', args, options);
    /*
     processing.on('message',function(message) {
     console.log('PROC: child message',this.pid,message);
     });
     */
    processing.on('error',function(err) {
      console.error(
        'PROC: child error',this.pid,err);
    });

    processing.on('exit',function(code, signal) {
      console.warn('PROC: child exit',this.pid,code, signal);
      sendServerAnswer(childProcess[this.pid], code, signal);
      delete childProcess[this.pid];
    });

    processing.on('disconnect',function(e) {
      console.warn('PROC: disconnect, cleanup',this.pid,code,signal);
      this.kill();
    });

    processing.stdout.setEncoding('utf8');
    processing.stderr.setEncoding('utf8');
    /*
     process.stdout.on('data', function (data) {
     //console.log('stdout: ' + data);
     data = data.toString('utf8');
     g_stdout.push(data);
     });
     */
    processing.stdout.on('data', function (data) {
      console.log('stdout: ' + data);
      data = data.toString('utf8');
      g_stdout.push(data);
      //childProcess[this._handle.owner].stdout.push(data);
      //outcb && outcb(data);
    });
    processing.stderr.on('data', function (data) {
      console.log('stderr: ' + data);
      data = data.toString('utf8');
      g_stdout.push(data);
      //childProcess[this.pid].stdout.push(data);
      //errcb && errcb(data);
    });


    testRun.process = processing;
    childProcess[processing.pid] = testRun;
    latestPid = processing.pid;
    console.log('run-test:forked', processing.pid, Object.keys(childProcess).length);
  }

  mkdirp(TEST_RESULTS_PATH, function (err) {
    if (err) console.error(err);
    else {
      fs.writeFile(scriptPath, req.body.testScript, function () {
        var cmd = './bin/nightwatch.js';
        var args = [cmd, '--env','chrome','--test', scriptPath];
        if (hasVideo) {
          args = [cmd, '--env','chrome_video','--test', scriptPath];
        }

        console.log('run-test:fork new process to run the script', cmd, args);
        runCount++;

        if (testRun.runOptions && testRun.runOptions.indexOf('#getjson') >= 0) {
          console.log('run-test: command #getjson');
          getWebRTCStat();
        }
        else {
          //var process = fork(cmd, args, {silent: true});
          var options = {
            cwd: path || process.cwd(),
            env: env,
            max: 1,
            //silent: true,
            // detached: true
          };

          g_stdout = [];
          spawnNightWatch(args, options);
          //callNightWatch(args, options);
        }
        res.status(200).send('run-test:forked');
      });
    }
  });
});

router.get('/hello', function(req, res) {
  res.status(200).send('agent-hello');
});

function getAdminURL(relativePath) {
  return (process.env.ADMIN_URL + relativePath)
}

router.get('/ping', function(req, res) {
  var url = getAdminURL('/api/agents/ping');

  request({
    method: 'GET',
    strictSSL: false,
    url: url
  }, function (error, response, body) {
    console.log('answer from admin server ping',error, body);

    if (error) return res.status(200).json({ status: false, version: '0.0.2', err: error, message: 'failed to connect to admin server', adminUrl: process.env.ADMINURL, url: url });
    return res.status(200).json({ status: true, version: '0.0.2', serverAnswer: body });
  });
});

router.post('/room-info/:varName', function(req, res) {
  var varName = req.params.varName;
  var varValue = req.body.varValue;

  var url = getAdminURL('/api/agents/room-info/' + process.env.RTC_SESSION_NAME + '/' + varName);
  console.log('post room-info BODY 2.0', JSON.stringify(req.body));
  console.log('post room-info', url, '=', varValue);

  request({
    method: 'POST',
    strictSSL: false,
    url: url,
    body: {
      value: varValue
    },
    json: true
  }, function (error, response, body) {
    console.log('answer from admin server post room-info',error, body);

    if (error) return res.status(404).json({ status: false, err: error });
    return res.status(200).json({ status: true });
  });
});

router.get('/room-info/:varName', function(req, res) {
  var varName = req.params.varName;
  var url = getAdminURL('/api/agents/room-info/' + process.env.RTC_SESSION_NAME + '/' + varName);
  console.log('get room-info', url);

  request({
    method: 'GET',
    strictSSL: false,
    url: url,
  }, function (error, response, body) {
    var json = JSON.parse(body);
    console.log('answer from admin server get room-info',error, json);

    if (error) return res.status(404).json({ status: false, err: error });
    return res.status(200).json(json);
  });
});

module.exports = router;

if (!process.env.ADMIN_URL) {
  console.error('ADMIN_URL environment variable is not set')
}