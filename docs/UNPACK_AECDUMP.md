# Getting unpack_aecdump binary

This tool is a part of webrtc project.
Some information on how to obtain webrtc sources and build them: http://webrtc.org/native-code/development/

## Simplest way

Prebuilt version can be downloaded from this repo: https://github.com/testRTC/unpack_aecdump

## Docker image

Docker image `akrn/unpack_aecdump` already contains necessary tools and webrtc source code as well as the binary.
Image can be pulled from the private TestRTC registry:

```
docker pull registry.testrtc.com:443/akrn/unpack_aecdump
```

The binary is in `/home/user/webrtc-checkout/src/out/Release` directory.

Dockerfile for this image is also available here: https://github.com/testRTC/unpack_aecdump