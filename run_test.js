(function () {
  'use strict';

  var debug = require('debug')('run_test');
  debug = console.info.bind(console);
  debug('run_test version 0.9.1');

  var testExecutions = require('./test-utils/test-executions');
  var path = require('path');

  function getUserHome() {
    var home = process.env.HOME || process.env.HOMEPATH || process.env.USERPROFILE;
    if (home === '/root') {
      home = '/home/seluser';
    }

    return home;
  }


  function listenToStdin() {
    var stdinData = '';
    process.stdin.on('readable', function () {
      //process.stdin.resume();
      var chunk = process.stdin.read();
      if (chunk !== null) {
        stdinData += chunk.toString();
        debug('process.stdin.on readable', '[' + stdinData + ']');

        try {
          // Try to parse stdin data
          JSON.parse(stdinData);
        } catch (e) {
          // Incomplete object - wait more data
          debug('Partial stdin, waiting for more');
          return;
        }

        testExecutions.parseStdin(stdinData);
        stdinData = '';
      }
    });

    process.stdin.on('end', function() {
      debug('process.stdin.on end');
      if (stdinData) {
        testExecutions.parseStdin(stdinData);
      }
    });
  }

  var webRTC_dir = path.resolve(getUserHome() + '/Downloads/');

  testExecutions.init({webRTC_dir: webRTC_dir});
  testExecutions.reportMemoryStatus();
  testExecutions.prepareTestEnv();
  listenToStdin();
}());
