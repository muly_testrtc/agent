var _ = require('lodash');
var fs = require('fs');
var co = require('co');
var Q = require('q');
var path = require('path');
var getJsonData = require('./shell-utils').loadConfig('./docker-data.json');
var shell = require('shelljs');
var spawn = require('child_process').spawn;
var parse = require('shell-quote').parse;
var executeRetry = require('./shell-utils').executeRetry;
var runDocker = require('./shell-utils').runDocker;
var build = require('./docker-build');

var dockerlib = require('dockerlib');
dockerlib.extraVerbose = true;
dockerlib.verbose = true;

var debug = require('debug')('prepare-machine');
var dry = require('debug')('dry');
debug = console.info.bind(console);
dry = console.info.bind(console);

function PrepareMachine(options) {
  this.runMode = options;
  this.errors = [];

  debug('prepare machine', this.runMode);
}

//don't use it overwrite the machine configuration /etc/default/docker
PrepareMachine.prototype.getMachineConfig = function(machineName) {
  var self = this;
  var getConfig;
  var cmd = 'docker-machine config ' + machineName;
  debug('CMD', cmd);
  if (!self.runMode.dry) {
    getConfig = executeRetry('get machine configuration ' + machineName,
      shell.exec.bind(shell, cmd), self.errors);
  } else {
    getConfig = {
      output: '<dry' + machineName + '>',
      code: 0
    };
  }
  console.log('Get machine configuration', machineName, getConfig);

  if (getConfig.code != 0) {
    throw new Error('get machine configuration ' + machineName + ' failed');
  }

  return getConfig.output;
};

PrepareMachine.prototype.pullOtherImages = function*(machineName) {
  var self = this;

  if (!self.runMode.dry) {
    var additionalImages = ['ubuntu:14.04', 'mulyoved/video-storage:v0.0.4'];

    for (var i=0; i<additionalImages.length; i++) {
      var imagename = additionalImages[i];
      if (!this.runMode.filter || imagename.indexOf(this.runMode.filter) >=0) {

        //it is fast enough I don't care to check if already exists
        //if (imagelist.output.indexOf(imagename) < 0) {
        //  debug('image not found, going to pull it', imagename);

        cmd = 'sudo docker pull ' + imagename;
        yield self.executeSSH('pull ' + machineName + ' ' + imagename, machineName, cmd);
      }
    }

    //For the record check which images the machine has
    cmd = 'sudo docker images';
    var imagelist = yield self.executeSSH('list images ' + machineName, machineName, cmd);

    debug('imagelist', imagelist);
  }
};

PrepareMachine.prototype.loadMachines = function*() {
  var self = this;

  function* loadMachine(data, machineName) {
    debug('loadMachine', machineName, data);
    var needisave = false;
    var file;
    var i;
    var files = shell.ls('/var/lib/jenkins/isave/');
    for (i=0; i<files.length; i++) {
      file = files[i];
      if (!self.runMode.filter || file.indexOf(self.runMode.filter) >=0) {
        needisave = true;
      }
    }

    yield self.executeSSH('confirm docker setup ' + machineName, machineName, 'sudo docker info');
    if (needisave) {
      yield self.executeSSH('remove all files from isave ' + machineName, machineName, 'rm /mnt/isave/*');
      yield self.executeSSH('list files in isave before copy' + machineName, machineName, 'ls -la /mnt/isave');

      yield self.executeShell('scp images ' + machineName, 'docker-machine scp -r /var/lib/jenkins/isave/ ' + machineName + ':/mnt');

      yield self.executeSSH('list files in isave after copy' + machineName, machineName, 'ls -la /mnt/isave');

      for (i = 0; i < files.length; i++) {
        file = files[i];
        if (!self.runMode.filter || file.indexOf(self.runMode.filter) >= 0) {
          yield self.executeSSH('load image ' + machineName + ' ' + file, machineName, 'sudo docker load -i /mnt/isave/' + file);
        }
      }
    }

    //var machineConnectInfo = self.getMachineConfig(machineName);
    yield self.pullOtherImages(machineName);
  }

  for (machineName in getJsonData.machines) {
    if (getJsonData.machines.hasOwnProperty(machineName)) {
      var data = getJsonData.machines[machineName];
      if (!data.disabled && (!self.runMode.machineName || machineName === self.runMode.machineName)) {
        yield loadMachine(data, machineName);
      }
    }
  }
};

/*
PrepareMachine.prototype.execute = function() {
  var self = this;

  function prepareMachine(data, machineName) {
    debug('prepareMachine', machineName, data);

    throw Error('machineConnectInfo cannot use, overwrite the machine configuration, use ssh instead');
    var machineConnectInfo = self.getMachineConfig(machineName);
    var _runMode = _.clone(self.runMode);
    _runMode.machineConnectInfo = machineConnectInfo;
    _runMode.machineName = machineName;
    _runMode.build = false;
    _runMode.pull = true;
    _runMode.taskName = machineName;

    var buildProcess = new build.BuildProcess(_runMode);
    buildProcess.execute();

    //check if need to pull misc containers
    self.pullOtherImages(machineConnectInfo, machineName);
  }

  for (machineName in getJsonData.machines) {
    if (getJsonData.machines.hasOwnProperty(machineName)) {
      var data = getJsonData.machines[machineName];
      if (!data.disabled && (!self.runMode.machineName || machineName === self.runMode.machineName)) {
        prepareMachine(data, machineName);
      }
    }
  }
};
*/

PrepareMachine.prototype.parseCert = function() {
  var getConfig;
  var cmd = 'docker-machine config ' + machineName;
  debug('CMD', cmd);
  if (!this.runMode.dry) {
    getConfig = executeRetry('get machine configuration ' + machineName,
      shell.exec.bind(shell, cmd), self.errors);
  } else {
    getConfig = '<dry' + machineName + '>';
  }

  console.log('Get machine configuration', machineName, getConfig);
  var machineConnectInfo = getConfig.output;
};

PrepareMachine.prototype.executeSSH = function*(msg, machineName, sshCmd) {
  var self = this;

  if (sshCmd.indexOf("'") !== 0 && sshCmd.indexOf('"') !== 0) {
    sshCmd = "'" + sshCmd + "'";
  }

  var cmd = 'docker-machine ssh ' + machineName + ' ' + sshCmd;
  return yield self.executeShell(msg, cmd);
};

PrepareMachine.prototype.execute = function(msg, cmd) {
  var deferred = Q.defer();

  //var args = parse(cmd);
  //var cmd0 = args.shift();

  var child = spawn('bash', ['-c', cmd]);

  child.stdout.on('data', function (data) {   process.stdout.write(data.toString());  });

  //spit stderr to screen
  child.stderr.on('data', function (data) {   process.stdout.write(data.toString());  });

  child.on('close', function (code) {
    console.log("Finished with code " + code);
    deferred.resolve(code);
  });

  return deferred.promise;
};


PrepareMachine.prototype.executeShell = function*(msg, cmd) {
  debug('executeShell', msg, cmd);
  var self = this;
  if (!self.runMode.dry) {
    var code = yield self.execute(msg, cmd);

    //executeRetry(msg,
    //  shell.exec.bind(shell, cmd, {silent:false}), self.errors);
  } else {
    var args = parse(cmd);
    dry(cmd, args);
  }
};

PrepareMachine.prototype.create = function*() {
  var self = this;

  function* createMachine(data, machineName) {
    debug('createMachine', machineName, data);

    var cmd =
      'docker-machine --debug create -d azure ' +
      '--azure-location "' +(data.location || 'East US') + '" ' +
      '--azure-subscription-id="94e4de7a-ea4e-4517-b553-eb9ca650a9b8" ' +
      '--azure-subscription-cert="mycert.pem" ' + machineName + ' ' +
      '--azure-size "Standard_A3"';

    yield self.executeShell('Create Machine ' + machineName, cmd);

    for (var port = 5900; port<=5904; port++) {
      cmd = 'azure vm endpoint create ' + machineName + ' ' + port + ' ' + port;
      yield self.executeShell('Open Port ' + machineName + ' ' + port, cmd);
    }

    //docker-machine ssh azure-agent-11 'sudo su root -c "echo \"echo DOCKER_OPTS=-H tcp://0.0.0.0:2376 -H unix:///var/run/docker.sock --storage-driver aufs --label provider=azure -g /mnt > /etc/default/docker\" | sudo bash"'
    var file = "DOCKER_OPTS=\\'-H tcp://0.0.0.0:2376 -H unix:///var/run/docker.sock --storage-driver aufs --label provider=azure -g /mnt\\'";
    var sshCmd = 'echo ' + file + ' | sudo tee /etc/default/docker';

    yield self.executeSSH('Configure Docker ' + machineName, machineName, '"' + sshCmd + '"');
    yield self.executeSSH('Restart Docker', machineName, 'sudo service docker restart');
    yield self.executeSSH('Configure Agent', machineName, "'sudo bash' < ./scripts/config-agent.sh");
    var answer = yield self.executeSSH('Confirm folder creation', machineName, 'ls -la /mnt/isave/');
    debug(answer);
    yield self.executeShell('Restart', 'docker-machine restart ' + machineName);
    yield self.executeSSH('confirm docker setup ' + machineName, machineName, 'sudo docker info');
    yield self.executeShell('Copy back machine setup to agent folder so can commit', 'cp -r /var/lib/jenkins/.docker/machine /var/lib/jenkins/agent/');

    debug('*** IMPORTANT ***');
    debug('Make sure to commit the new machine setup from ~/agent');
    debug('*** IMPORTANT ***');
  }


  for (machineName in getJsonData.machines) {
    if (getJsonData.machines.hasOwnProperty(machineName)) {
      var data = getJsonData.machines[machineName];
      if (!data.disabled && (!self.runMode.machineName || machineName === self.runMode.machineName)) {
        yield createMachine(data, machineName);
      }
    }
  }
};

PrepareMachine.prototype.test = function*() {
  var self = this;

  debug('testing');
};

exports.PrepareMachine = PrepareMachine;