'use strict';

var _ = require('lodash');
var fs = require('fs');
var co = require('co');
var path = require('path');
const normalizeNewline = require('normalize-newline');

var getJsonData = require('./shell-utils').loadConfig('./docker-data.json');
var shell = require('shelljs');
var executeRetry = require('./shell-utils').executeRetry;
var runDocker = require('./shell-utils').runDocker;

var dockerlib = require('dockerlib');
dockerlib.extraVerbose = true;
dockerlib.verbose = true;

var debug = require('debug')('docker-build');
var dry = require('debug')('dry');
debug = console.info.bind(console);
dry = console.info.bind(console);
var registryURL = 'registry.testrtc.com:443';

function BuildProcess(options) {
  this.runMode = options;
  this.browserVersions = { errors: [] };
  this.pullScript = '#! /bin/bash\n\nset -e\n';
  debug('docker', this.runMode);
  this.machineConnectInfo = options.machineConnectInfo;
}

BuildProcess.prototype.execute = function() {
  var self = this;

  if (this.runMode.build && this.runMode.pull) {
    throw new Error('Invalid working mode, cannot do both build & pull')
  }

  if (!shell.test('-d', 'dist')) {
    shell.mkdir('dist');
  }

  if ((this.runMode.push || this.runMode.pull) && !this.runMode.dry && !this.runMode.save) {
    debug('docker login');

    //registry.testrtc.com:443
    //testrtc
    //NyM8TG62nodazHjbgr
    //muly@testrtc.com
    //docker login --username="testrtc" registry.testrtc.com:443
    //docker tag mulyoved/media-convert:v1.8.3 registry.testrtc.com:443/mulyoved/media-convert:v1.8.3
    //docker push registry.testrtc.com:443/mulyoved/media-convert:v1.8.3
    //--disable-content-trust=true
    //docker pull registry.testrtc.com:443/mulyoved/media-convert:v1.8.4


    //dockerlib.docker.login('testrtc', registryURL);
  }

  /*
  if (!this.runMode.filter || this.runMode.filter === 'convert') {
    this.add('convert', 'Media Convert', 'images/MediaConvert/Dockerfile', null, 'mulyoved/media-convert:' + getJsonData.image.version, true);
  }
  */

  _.each(getJsonData.chrome, function(data, version) {
    if (!data.disabled) {
      self.buildBrowserVersion('chrome', version, data, self.machineConnectInfo);
    }
  });

  _.each(getJsonData.firefox, function(data, version) {
    if (!data.disabled) {
      self.buildBrowserVersion('firefox', version, data, self.machineConnectInfo);
    }
  });

  fs.writeFileSync('dist/pull.sh', this.pullScript); // just in case we want it manually
  debug('* Pull Script *--------------------------');
  debug(this.pullScript);
  debug('* Pull Script *--------------------------');

  if (this.runMode.build) {
    fs.writeFileSync('dist/build-log.json', JSON.stringify(this.browserVersions, null, 2));
    debug('Process Completed, browser versions:', JSON.stringify(this.browserVersions, null, 2));
  } else {
    debug('Process Completed, errors', JSON.stringify(this.browserVersions.errors, null, 2));
  }
};

BuildProcess.prototype.add = function(version, name, dockerPath, path, imagename, isPush) {
  var imagenameNoTag = imagename.substring(0, imagename.indexOf(':')).replace('/', '-');
  if (this.runMode.build) {
    debug('Build Image', version, name, dockerPath, path, imagename, isPush);
    if (path && !this.runMode.dry) {
      shell.pushd(path);
    }
    debug('Build ' + name + ' ' + imagename);
    if (!this.runMode.dry) {
      executeRetry('build:' + imagename, runDocker.bind(this, 'build --no-cache -f ' + dockerPath + ' -t ' + imagename + ' .'), this.browserVersions.errors);
    } else {
      dry('docker build', path, imagename, dockerPath);
    }

    if (path && !this.runMode.dry) {
      shell.popd();
    }
  }

  if (this.runMode.push && isPush) {
    if (!this.runMode.dry) {
      if (this.runMode.save) {
        executeRetry('save ' + imagename, runDocker.bind(this, 'save -o /var/lib/jenkins/isave/' + imagenameNoTag + ' ' + imagename), this.browserVersions.errors);
      } else {
        //executeRetry('tag ' + imagename, dockerlib.docker.tag.bind(dockerlib.docker, imagename, registryURL + '/' + imagename), this.browserVersions.errors);
        executeRetry('tag ' + imagename, runDocker.bind(this, 'tag --force=true ' + imagename + ' ' + registryURL + '/' + imagename), this.browserVersions.errors);
        executeRetry('push ' + imagename, dockerlib.docker.push.bind(dockerlib.docker, registryURL + '/' + imagename), this.browserVersions.errors);
      }
    } else {
      dry('docker ' + this.runMode.save ? 'save' : 'push', imagename)
    }
  }

  if (this.runMode.pull && isPush && !this.runMode.save) {
    if (!this.runMode.dry) {
      var cmd = this.machineConnectInfo + ' pull ' + registryURL + '/' + imagename;
      executeRetry('pull ' + registryURL + '/' + imagename, runDocker.bind(this, cmd), this.browserVersions.errors);

      cmd = this.machineConnectInfo + ' tag ' + registryURL + '/' + imagename + ' ' + imagename;
      executeRetry('tag ' + imagename, runDocker.bind(this, cmd), this.browserVersions.errors);
    } else {
      dry('docker pull', this.machineConnectInfo, imagename)
    }
  }

  if (isPush) {
    this.pullScript += '\necho "*Pull ' + name + ' ' + imagename + '"\ndocker pull ' + imagename + '\n';
  }
};

BuildProcess.prototype.buildBrowserVersion = function(browser, version, data, machineConnectInfo) {
  function processFile(file, outFolder) {
    //debug('processFile', file, outFolder);
    var text = fs.readFileSync('image-build/' + file, 'utf8');

    function replace(search, replace) {
      text = text.split(search).join(replace);
    }

    var agentVersion = process.env.AGENT_VERSION;
    //console.log('Build agent version', agentVersion);

    replace('{{browser}}', browser);
    replace('{{base.version}}', getJsonData.base.version);
    replace('{{image.version}}', agentVersion);
    replace('{{CHROME_DRIVER_VERSION}}', getJsonData.base.CHROME_DRIVER_VERSION);
    replace('{{chrome.version}}', version);
    replace('{{folder-name}}', browser + '-' + version);
    replace('{{download.url}}', data.url);
    replace('{{version-ext}}', versionExt);
    replace('{{image-name}}', imageName);

    var outPath = outFolder + '/' + file;
    shell.mkdir('-p', path.dirname(outPath));
    fs.writeFileSync(outPath, normalizeNewline(text));
  }

  if (!this.runMode.dry) {
    debug('Process Browser', browser, version);
  }

  var agentVersion = process.env.AGENT_VERSION;

  if (!this.runMode.filter || this.runMode.filter === browser + '-' + version) {
    var versionExt = data['version-ext'] ? '-' + data['version-ext'] : '';
    var imageName = 'mulyoved/node-' + browser + '-' + version + ':' + agentVersion;
    var runImageName = 'mulyoved/' + browser + '-' + version + ':' + agentVersion;
    var debugImageName = 'mulyoved/' + browser + '-debug-' + version + ':' + agentVersion;

    var dockerFolder = browser === 'chrome' ? 'NodeChrome' : 'NodeFirefox';
    var notDockerFolder = browser !== 'chrome' ? 'NodeChrome' : 'NodeFirefox';
    var distFolder = 'dist/' + browser + '-' + version + '/';
    var browserCmd = browser === 'chrome' ? '/opt/google/chrome' + versionExt + '/chrome --product-version' : '/usr/bin/firefox --version';



    if (this.runMode.build) {
      var files = shell.ls('-R', 'image-build');
      _.each(files, function (file) {
        if (!shell.test('-d', 'image-build/' + file)) {
          if (file.indexOf(browser !== 'chrome' ? 'NodeChrome' : 'NodeFirefox') < 0) {
            processFile(file, 'dist/' + browser + '-' + version);
          }
        }
      });
    }

    this.add(version, 'Node ' + browser, data.url ? 'Dockerfile.imageurl' : 'Dockerfile', distFolder + dockerFolder, imageName, false);
    this.add(version, 'Standalone ' + browser, distFolder + 'Standalone/Dockerfile', '', runImageName, true);
    this.add(version, 'Standalone Debug ' + browser, distFolder + 'StandaloneDebug/Dockerfile', '', debugImageName, true);

    var brwserVersion = 'unknown-dry-' + version;
    if (this.runMode.build && !this.runMode.dry) {
      var cmdline = 'docker run ' + imageName + ' ' + browserCmd;
      var result = shell.exec(cmdline, {silent: (!dockerlib.verbose || dockerlib.sudo)});
      if (result.code !== 0) {
        throw new Error("Command [" + cmdline + "] returned code " + result.code);
      }
      brwserVersion = result.output;
      debug('Browser Version', brwserVersion);
    }

    this.browserVersions[browser + '-' + version] = {
      id: browser + '-' + version,
      browser: browser,
      image: imageName,
      runImageName: runImageName,
      debugImageName: debugImageName,
      version: brwserVersion
    }
  }
};

exports.BuildProcess = BuildProcess;