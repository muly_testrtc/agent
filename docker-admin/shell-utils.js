var _ = require('lodash');
var fs = require('fs');
var path = require('path');
var shell = require('shelljs');

var dockerlib = require('dockerlib');
dockerlib.extraVerbose = true;
dockerlib.verbose = true;

debug = require('debug')('sheel-utils');
debug = console.info.bind(console);

//Copy from https://github.com/reyesr/dockerlib/blob/master/index.js
function runDocker(args) {
  var cmdline = "docker " + args;
  debug("exec: ", cmdline);
  var result = shell.exec(cmdline, {silent:(!exports.verbose||exports.sudo)});
  if (result.code !== 0) {
    debug(result.output);
    throw new Error("Command [" + cmdline + "] returned code " + result.code);
  }
  debug("completed - exec: " + cmdline);
}

function executeRetry(opName, op, errors) {
  var answer;
  var maxRetry = 1000;
  for (var retryIdx=0; retryIdx<maxRetry; retryIdx++) {
    debug(opName, 'execute retry=', retryIdx);

    try {
      answer = op();
      var code = 0;
      var ok = true;
      if (answer && answer.code) {
        code = answer.code;
        if (answer.output) {
          if (answer.output.indexOf('Error:')>=0) {
            debug('* Going to retry, answer contain error', answer);
            ok = false;
          }
          if (answer.output.indexOf('lost connection')>=0) {
            debug('* Going to retry, answer contain error', answer);
            ok = false;
          }
        }
      }
      if (ok) {
        debug('Completed Execute', opName, 'successfully answer:', answer);
        debug('-------------------------------------------------');
        break;
      }
    } catch(err) {
      debug('Failed Execute', opName, err.stack);
      if (retryIdx === maxRetry - 1) {
        debug('Permanently Failed to Execute', opName);
        debug('-------------------------------------------------');
        errors.push(opName);
      } else {
        debug('Failed to Execute, going to retry', opName);
        debug('---');
      }
    }
  }

  return answer;
}

/*! JSON.minify()
 v0.1 (c) Kyle Simpson
 MIT License
 */

//(function(){

function minfiJson(json) {

  var tokenizer = /"|(\/\*)|(\*\/)|(\/\/)|\n|\r/g,
    in_string = false,
    in_multiline_comment = false,
    in_singleline_comment = false,
    tmp, tmp2, new_str = [], ns = 0, from = 0, lc, rc
    ;

  tokenizer.lastIndex = 0;

  while (tmp = tokenizer.exec(json)) {
    lc = RegExp.leftContext;
    rc = RegExp.rightContext;
    if (!in_multiline_comment && !in_singleline_comment) {
      tmp2 = lc.substring(from);
      if (!in_string) {
        tmp2 = tmp2.replace(/(\n|\r|\s)*/g,"");
      }
      new_str[ns++] = tmp2;
    }
    from = tokenizer.lastIndex;

    if (tmp[0] == "\"" && !in_multiline_comment && !in_singleline_comment) {
      tmp2 = lc.match(/(\\)*$/);
      if (!in_string || !tmp2 || (tmp2[0].length % 2) == 0) {	// start of string with ", or unescaped " character found to end string
        in_string = !in_string;
      }
      from--; // include " character in next catch
      rc = json.substring(from);
    }
    else if (tmp[0] == "/*" && !in_string && !in_multiline_comment && !in_singleline_comment) {
      in_multiline_comment = true;
    }
    else if (tmp[0] == "*/" && !in_string && in_multiline_comment && !in_singleline_comment) {
      in_multiline_comment = false;
    }
    else if (tmp[0] == "//" && !in_string && !in_multiline_comment && !in_singleline_comment) {
      in_singleline_comment = true;
    }
    else if ((tmp[0] == "\n" || tmp[0] == "\r") && !in_string && !in_multiline_comment && in_singleline_comment) {
      in_singleline_comment = false;
    }
    else if (!in_multiline_comment && !in_singleline_comment && !(/\n|\r|\s/.test(tmp[0]))) {
      new_str[ns++] = tmp[0];
    }
  }
  new_str[ns++] = rc;
  return new_str.join("");
};
//})(this);

function loadConfig(fileName) {
  return JSON.parse(minfiJson(fs.readFileSync('./' + fileName, 'utf8')));
}


exports.executeRetry = executeRetry;
exports.runDocker = runDocker;
exports.minfiJson = minfiJson;
exports.loadConfig = loadConfig;