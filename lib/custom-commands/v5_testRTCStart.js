function testRTCStart() {
}

testRTCStart.prototype.command = function() {
  var self = this;
  var fileCount = 0;
  var mainWindowHandle = 0;


  self.client.api.mediaAgent('webrtc-stat-clear', null, function(result) {
    console.log('testRTCStart:Clear logs->', result);
  })

    .url('chrome://webrtc-internals/')
    //open the webrtc stat in a new tab, otherwise it replace the current tab and clear the stat values
    .execute(function() {
      window.open('');
    }
    ,[0],
    function(result) {
    })

    .window_handles(function(result) {
      //console.log('handles',result);
      var handle = result.value[1];
      self.client.api.switchWindow(handle);
    })
    .windowHandle(function(handle) {
      //console.log('Current window handle->', handle.value);
    });

  return this;
};

module.exports = testRTCStart;