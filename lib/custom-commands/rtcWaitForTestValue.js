var util = require('util');
var events = require('events');
//var TestConstants = require('../pageObjects/testConstants.js');

/*
 * This custom command allows us to locate an HTML element on the page and then wait until the value of a specified
 * attribute matches the provided expression (aka. the 'checker' function). It retries executing the checker function
 * every 100ms until either it evaluates to true or it reaches maxTimeInMilliseconds (which fails the test).
 * Nightwatch uses the Node.js EventEmitter pattern to handle asynchronous code so this command is also an EventEmitter.
 */

function WaitForSessionInfo() {
  events.EventEmitter.call(this);
  this.startTimeInMilliseconds = null;
}

util.inherits(WaitForSessionInfo, events.EventEmitter);

WaitForSessionInfo.prototype.command = function (varName, cb, timeoutInMilliseconds) {
  this.startTimeInMilliseconds = new Date().getTime();
  var self = this;
  var message;

  if (typeof timeoutInMilliseconds !== 'number') {
    timeoutInMilliseconds = this.api.globals.waitForConditionTimeout;
  }

  this.check(varName, function (result, loadedTimeInMilliseconds) {
    if (result) {
      message = 'rtcWaitForTestInfo: [' + varName + '='+ result + '] received after ' + (loadedTimeInMilliseconds - self.startTimeInMilliseconds) + ' ms.';
    } else {
      message = 'rtcWaitForTestInfo: [' +varName+'] was not found in ' + timeoutInMilliseconds + ' ms.';
    }

    //console.info("TEST_RTC_ERROR:", message);
    //self.client.assertion(result, 'expression false', 'expression true', message, true);
    if (cb) {
      cb(result);
    }
    self.emit('complete');
  }, timeoutInMilliseconds);

  return this;
};

WaitForSessionInfo.prototype.check = function (varName, cb, maxTimeInMilliseconds) {
  var self = this;

  this.api.rtcGetTestInfo(varName, function (result) {
    var now = new Date().getTime();
    if (result) {
      if (cb) {
        cb(result, now);
      }
    } else if (now - self.startTimeInMilliseconds < maxTimeInMilliseconds) {
      setTimeout(function () {
        self.check(varName, cb, maxTimeInMilliseconds);
      }, 1000 /*TestConstants.TIMEOUT_RETRY_INTERVAL*/);
    } else {
      cb(null);
    }
  });
};

module.exports = WaitForSessionInfo;