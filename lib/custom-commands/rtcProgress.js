var util = require('util');
var events = require('events');

function rtcProgress() {
  events.EventEmitter.call(this);
}

util.inherits(rtcProgress, events.EventEmitter);

rtcProgress.prototype.command = function() {
  var self = this;
  console.info('TestScriptProgress: ' + util.format.apply(util, arguments));

  console.info(util.format('TEST-RTC-MESSAGE: %s', JSON.stringify({
    msgType: 'progress',
    text: util.format.apply(util, arguments)
  })));


  process.nextTick(function() {
    self.emit('complete');
  });

  return this;
};

module.exports = rtcProgress;