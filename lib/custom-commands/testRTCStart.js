var util = require('util');
var events = require('events');
var fs = require('fs');
var path = require('path');
var os = require('os');
var rtcTestInfo = require('../../test-utils/rtcTestInfo');

function testRTCStart() {
  events.EventEmitter.call(this);
}

util.inherits(testRTCStart, events.EventEmitter);

function getBlankPageUriWithOptions(extensionOptions) {
  var extensionOptionsEncoded = encodeURIComponent(JSON.stringify(extensionOptions, null, 0));
  return 'file://' + path.join(__dirname, '../../test-utils/blank.html') + '?TESTRTC_OPTIONS=' + extensionOptionsEncoded;
}

testRTCStart.prototype.command = function() {
  var self = this;
  var fileCount = 0;
  var mainWindowHandle = 0;

  var browserEnv = rtcTestInfo.getTestOptions().browserEnv;
  var useGetStat = rtcTestInfo.getTestOptions().useGetStat;
  var isFireFox = browserEnv.indexOf('firefox')>=0;

  if (isFireFox) {
    var extensionOptions = { videoURL: 'http://localhost:8888/stream', videoMimeType: this.api.globals.fakeVideoMimeType };

    this.client.api
      .rtcInfo('Firefox mode')
      .url(getBlankPageUriWithOptions(extensionOptions)).pause(500)

      .perform(function() {
        self.emit('complete');
      });
  } else {
    var extensionOptions = { useGetStat: useGetStat };

    self.client.api
      // This blank file we use to pass options to our extension
      .url(getBlankPageUriWithOptions(extensionOptions)).pause(500)

      .url('chrome://webrtc-internals/')
      //open the webrtc stat in a new tab, otherwise it replace the current tab and clear the stat values
      .execute(function() { window.open(''); })

      .window_handles(function (result) {
        var handle = result.value[1];
        // Switch window and initialise extension with test options by sending a message it listens for
        self.client.api.switchWindow(handle);
      })
      .windowHandle(function (result) {
        // Do nothing?
      })
      .perform(function(client, done1) {
        if (process.env.AUDIO_CAPTURE) {
          self.client.api
            .rtcStartAudioRecording()
            .perform(function(client, done2) {
              // TODO: fix this
              done1();
              done2();
              self.emit('complete');
            });
        } else {
          done1();
          self.emit('complete');
        }
      });
  }

  return this;
};

module.exports = testRTCStart;