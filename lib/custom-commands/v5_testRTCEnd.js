var util = require('util');
var events = require('events');

function testRTCEnd() {
  events.EventEmitter.call(this);
}

util.inherits(testRTCEnd, events.EventEmitter);

testRTCEnd.prototype.command = function() {
  var self = this;
  var fileCount = 0;
  var logs = [];

  self.client.api
    .getLog('browser', function(logEntriesArray) {
      console.log('LOGS:', logEntriesArray);
      logs = logEntriesArray;

      self.client.api
        .mediaAgent('webrtc-stat-clear', null, function(result) {
          //console.log('testRTCEnd:Clear logs->', result);
        });

        //Split as need to fill logs before execute this part
        self.client.api
          .window_handles(function(result) {
            //console.log('testRTCEnd: handles',result);
            var handle = result.value[0];
            self.client.api.switchWindow(handle);
          })

          .click("summary")
          .click('a button')
          .mediaAgent('get-webrtc-stat', logs, function(result) {
            //cb.call(self.client.api, result);
            self.emit('complete');
          });
    });

  return this;
};

module.exports = testRTCEnd;