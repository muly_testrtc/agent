var util = require('util');
var events = require('events');
var request = require('request');

function SetSessionInfo() {
  events.EventEmitter.call(this);
}

util.inherits(SetSessionInfo, events.EventEmitter);

SetSessionInfo.prototype.command = function(varName, varValue, cb) {
  var self = this;

  var url = process.env.ADMIN_URL + '/api/agents/room-info/' + process.env.RTC_TEST_NAME + '/' + varName + '/';
  console.log('SetSessionInfo ->', varName, varValue, url);

  request({
    method: 'POST',
    strictSSL: false,
    url: url,
    body: {
      value: varValue
    },
    json: true
  }, function (error, response, body) {
    console.log('SetTestInfo <-', error, body);
    if (cb) {
      cb.call(self.client.api, body);
    }
    self.emit('complete');
  });
  return this;
};

module.exports = SetSessionInfo;
