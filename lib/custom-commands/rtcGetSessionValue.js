var util = require('util');
var events = require('events');
var request = require('request');
var rtcTestInfo = require('../../test-utils/rtcTestInfo');

function rtcGetSessionValue() {
  events.EventEmitter.call(this);
}

util.inherits(rtcGetSessionValue, events.EventEmitter);

rtcGetSessionValue.prototype.command = function(varName, cb) {
  var self = this;
  var value = rtcTestInfo.getSessionValue(varName);
  console.log(util.format('rtcGetSessionValue -> varName:[%s] value:[%s]', varName, value));

  if (cb) {
    cb.call(self.client.api, value);
  }

  process.nextTick(function() {
    self.emit('complete');
  });

  return this;
};

module.exports = rtcGetSessionValue;
