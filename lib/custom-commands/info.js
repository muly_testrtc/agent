var util = require('util');
var events = require('events');

function rtcInfo() {
  events.EventEmitter.call(this);
}

util.inherits(rtcInfo, events.EventEmitter);

rtcInfo.prototype.command = function() {
  var self = this;

  this.client.api.executeAsync(function(data, done) {
      var arr = Object.keys(data).map(function (key) {return data[key]});
      arr.unshift('_testRTC_');
      console.info.apply(console, arr);
      done(true);
  },
  [arguments],
  function (result) {
    var arr = Object.keys(arguments).map(function (key) {return arguments[key]});
    arr.unshift('TestScriptInfo:');
    console.info.apply(console, arr);
    self.emit('complete');
  });

  return this;
};

module.exports = rtcInfo;