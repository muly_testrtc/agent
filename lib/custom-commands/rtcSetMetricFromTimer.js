var util = require('util');
var events = require('events');
var rtcTestInfo = require('../../test-utils/rtcTestInfo');

function rtcSetMetricFromTimer() {
  events.EventEmitter.call(this);
}

util.inherits(rtcSetMetricFromTimer, events.EventEmitter);

rtcSetMetricFromTimer.prototype.command = function(metricName, timerName, callback) {
  var self = this;

  timerName = timerName || metricName;

  var val = 0;
  var timers = rtcTestInfo.getTimers();
  var timerStart = timers[timerName];
  if (timerStart) {
    var now = (new Date()).getTime();
    val = now - timerStart;
  } else {
    val = 0;
  }

  rtcTestInfo.addValue('metric', metricName, {op: 'set', val: val, agg: 'avg'});
  if (callback) {
    callback.call(self, val);
  }

  process.nextTick(function() {
    self.emit('complete');
  });
  return this;
};

module.exports = rtcSetMetricFromTimer;