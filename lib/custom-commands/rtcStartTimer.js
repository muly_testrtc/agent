var util = require('util');
var events = require('events');
var rtcTestInfo = require('../../test-utils/rtcTestInfo');

function rtcStartTimer() {
  events.EventEmitter.call(this);
}

util.inherits(rtcStartTimer, events.EventEmitter);

rtcStartTimer.prototype.command = function(timerName) {
  var self = this;
  var timers = rtcTestInfo.getTimers();
  timers[timerName] = (new Date()).getTime();

  process.nextTick(function() {
    self.emit('complete');
  });
  return this;
};

module.exports = rtcStartTimer;