var util = require('util');
var events = require('events');
var sanitize = require("sanitize-filename");
var path = require('path');
var fs = require('fs');
var rtcTestInfo = require('../../test-utils/rtcTestInfo');

function rtcScreenshot() {
  events.EventEmitter.call(this);
}

rtcScreenshot.failureScreenName = 'Failure';
rtcScreenshot.counter = 1;
rtcScreenshot.limitReached = false;
rtcScreenshot.getNextPrefix = function() {
  /**
   * This static method returns string prefix for screenshot file
   *  based on the chronological order
   */
   var prefix = ('000' + rtcScreenshot.counter).slice(-3) + '_';
   rtcScreenshot.counter++;
   return prefix;
};

util.inherits(rtcScreenshot, events.EventEmitter);

rtcScreenshot.prototype.command = function(screenName, cb) {
  var self = this;

  var screenshotLimit = rtcTestInfo.getTestOptions().screenshotLimit;
  if (screenName !== rtcScreenshot.failureScreenName && !isNaN(screenshotLimit) && rtcScreenshot.counter - 1 >= screenshotLimit) {
    // Limit of screenshots has been reached
    if (!rtcScreenshot.limitReached) {
      // Issue a warning just one time
      rtcTestInfo.addMessage('TEST-RTC-WARN', 'warn', 'You hit screenshot limit which is set to ' + screenshotLimit + ' for your account');
    }
    rtcScreenshot.limitReached = true;

    process.nextTick(function() {
      if (typeof cb === 'function') {
        cb();
      }
      self.emit('complete');
    });

    return this;
  }

  var name = (typeof screenName === 'string' && screenName) ? sanitize(screenName) : getDatePattern();

  var filename = rtcScreenshot.getNextPrefix() + name + '.png',
      localPath = path.join(process.env.TEST_RESULTS_PATH, filename);

  // Get the current URL
  this.client.api.url(function(result) {
    var url = result.value;

    self.client.api.saveScreenshot(localPath, function(result) {
      rtcTestInfo.addScreenshot(filename, url);
      if (typeof cb === 'function') {
        cb();
      }
      self.emit('complete');
    });
  })

  return this;
};

function getDatePattern() {
  return new Date().toTimeString().split(' ')[0];
}

module.exports = rtcScreenshot;
