function StartRTCStatistic() {
}

StartRTCStatistic.prototype.command = function() {
  var self = this;
  var fileCount = 0;
  var mainWindowHandle = 0;


  self.client.api.mediaAgent('webrtc-stat-clear', null, function(result) {
    console.log('StartRTCStatistic:Clear logs->', result);
  })

  .url('chrome://webrtc-internals/')
    //open the webrtc stat in a new tab, otherwise it replace the current tab and clear the stat values
    .execute(function() {
      window.open('');
    }
    ,[0],
    function(result) {
    })

    .window_handles(function(result) {
      //console.log('handles',result);
      var handle = result.value[1];
      self.client.api.switchWindow(handle);
    })
    .windowHandle(function(handle) {
      //console.log('Current window handle->', handle.value);
    });

    /*

  .windowHandle(function(_mainWindowHandle) {
      mainWindowHandle = _mainWindowHandle.value;
      console.log('mainWindowHandle->', mainWindowHandle);
    })

    //open the webrtc stat in a new tab, otherwise it replace the current tab and clear the stat values
  .execute(function() {
      window.open('');
    }
    ,[0],
    function(result) {
    })

  .window_handles(function(result) {
      console.log('handles',result);
      var handle = result.value[1];
      self.client.api.switchWindow(handle);
  })
    .windowHandle(function(handle) {
      console.log('Current window handle->', handle.value);
    })


  .url('chrome://webrtc-internals/')
    .pause(1000, function() {
      console.log('waited 1 sec');
    })
  .switchWindow(mainWindowHandle)
  .pause(1000, function() {
    console.log('waited 1 sec');
  })

    .window_handles(function(result) {
      console.log('handles',result);
      var handle = result.value[0];
      self.client.api.switchWindow(handle);
    })
    .windowHandle(function(handle) {
      console.log('Current window handle->', handle.value);
    })

    .windowHandle(function(handle) {
      console.log('After switch back->', handle.value);
    });
    */

  return this;
};

module.exports = StartRTCStatistic;