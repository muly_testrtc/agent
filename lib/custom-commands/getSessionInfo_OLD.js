var util = require('util');
var events = require('events');
var request = require('request');

function GetSessionInfo() {
  events.EventEmitter.call(this);
}

util.inherits(GetSessionInfo, events.EventEmitter);

GetSessionInfo.prototype.command = function(varName, cb) {
  var self = this;

  var url = process.env.ADMIN_URL + '/api/agents/room-info/' + process.env.RTC_SESSION_NAME + '/' + varName + '/';
  console.log(util.format('GetSessionInfo -> varName:[%s], url:[%s]', varName, url));

  request({
    method: 'GET',
    strictSSL: false,
    url: url,
  }, function (error, response, body) {
    var value = null;
    console.log('GetSessionInfo <-', error, body);

    if (!error) {
      try {
        var json = JSON.parse(body);
        value = json.value;
      } catch (err) {
        console.error('GetSessionInfo: Failed to parse server results: [' + body + ']');
      }
    }
    if (cb) {
      cb.call(self.client.api, value);
    }
    self.emit('complete');
  });

  return this;
};

module.exports = GetSessionInfo;
