function AudioStart() {
}

AudioStart.prototype.command = function(command, cb) {
  var self = this;

  command = command || 'message';
  this.client.api.mediaAgent('audio-start/'+command, null, function(result) {
    console.log('AudioStart ->', result);
  });

  return this;
};

module.exports = AudioStart;