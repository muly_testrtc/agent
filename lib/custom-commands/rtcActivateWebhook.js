var util = require('util');
var events = require('events');
var request = require('request');

function rtcActivateWebhook() {
  events.EventEmitter.call(this);
}

util.inherits(rtcActivateWebhook, events.EventEmitter);

/**
 * The first argument can be either an URL or an options object (the same as node.js request accepts)
 * The second argument either a request body or a callback function
 * If request body is specified, method is set to POST
 *
 * In case options are specified as the first argument, supported properties can be found in request documentations:
 *  https://github.com/request/request#requestoptions-callback
 */
rtcActivateWebhook.prototype.command = function(url, json, cb) {
  var self = this;

  // Some default options
  var options = {
    strictSSL: false
  };

  if (typeof json === 'function') {
    cb = json;
    json = null;
  }

  if (typeof url === 'object') {
    options = url;
  } else {
    options.url = url;
  }

  if (json) {
    options.body = json;
    options.method = 'POST';
  }

  request(options, function (error, response, body) {
    console.log('rtcActivateWebhook <-', error, body.length < 1024 ? body : 'Large response of size: ' + body.length);
    if (typeof cb === 'function') {
      cb.call(self.client.api, body);
    }
    self.emit('complete');
  });
  return this;
};

module.exports = rtcActivateWebhook;
