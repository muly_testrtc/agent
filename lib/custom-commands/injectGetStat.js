var util = require('util');
var events = require('events');
var fs = require('fs');

var rtcTestInfo = require('../../test-utils/rtcTestInfo');

function injectGetStat() {
  events.EventEmitter.call(this);
}

util.inherits(injectGetStat, events.EventEmitter);

injectGetStat.prototype.command = function() {
  var self = this;
  var useGetStat = rtcTestInfo.getTestOptions().useGetStat;

  if (!useGetStat) {
    console.log('Skipping injectGetStat');
    process.nextTick(function() {
      self.emit('complete');
    });
    return this;
  }

  this.client.api.executeAsync(injectGetStatInterval,
  [],
  function (result) {
    self.emit('complete');
  });

  return this;
};

module.exports = injectGetStat;


/**
 * This function is supposed to be injected into the page
 */
function injectGetStatInterval(done) {
  if (!('___testRTCExtension' in window) || !('___testRTCTestAgent' in window)) {
    // Something went wrong
    throw new Error("The TestRTC API was not injected properly");
  }

  setInterval(function() {
    for (var channelId in ___testRTCTestAgent.knownChannels) {
      // Iterate over active connections
      var conn = ___testRTCTestAgent.knownChannels[channelId];
      if (!conn) {
        continue;
      }
      
      if (___testRTCExtension['mozRTCPeerConnection']) {
        mozGetStats(conn, channelId);
      } else if (___testRTCExtension['webkitRTCPeerConnection']) {
        webkitGetStats(conn, channelId);
      }
    }
  }, 1000);

  function webkitGetStats(conn, channelId) {
    var getStats = ___testRTCExtension.webkitGetStats.bind(conn);
    getStats(function(response) {
      /**
       * This is a raw version of Chrome getStats
       * TODO: this should be changed once https://bugs.chromium.org/p/webrtc/issues/detail?id=2031 is fixed
       */
      var standardReport = {};
      var reports = response.result();
      reports.forEach(function(report) {
        var standardStats = {
          id: report.id,
          timestamp: report.timestamp,
          type: report.type
        };
        report.names().forEach(function(name) {
          standardStats[name] = report.stat(name);
        });
        standardReport[standardStats.id] = standardStats;
      });

      appendStats(conn, channelId, standardReport, 'webkit');
    });
  }

  function mozGetStats(conn, channelId) {
    var getStats = ___testRTCExtension.mozGetStats.bind(conn);

    getStats(null, function(results) {
      appendStats(conn, channelId, results, 'firefox');
    }, function(err) {
      // Do nothing on error
    });
  }

  var WEBKIT_CHANNEL_PATTERN = /^(ssrc_\w+_(recv|send))|(Conn-(data|audio|video)-\w+.*)$/i,
      FIREFOX_CHANNEL_PATTERN = /^(in|out)bound_rtp/;

  /**
   * This function handles raw GetStats data and consolidates it
   *  in order to keep stat file small
   */
  function appendStats(conn, channelId, statItems, client) {
    if (!(channelId in ___testRTCTestAgent.stats)) {
      var extra = ___testRTCTestAgent.extra[channelId] || {};

      // The first record for this channelId
      ___testRTCTestAgent.stats[channelId] = {
        channelId: channelId,
        time: [],
        stat: {},
        extra: extra, // TODO: move to retrieveGetStat.js
        client: client
      };
    }

    // Shortcut reference
    var channelStats = ___testRTCTestAgent.stats[channelId];
    // How many samples have been recorded for the current channel
    var previousSampleCount = channelStats.time.length;

    // Get "subchannel" names
    var subChannelNames = Object.keys(statItems).filter(function(k) {
      if (!statItems.hasOwnProperty(k) || typeof statItems[k] !== 'object') {
        return false;
      }
      
      return (client === 'firefox' && FIREFOX_CHANNEL_PATTERN.test(k)) || (client === 'webkit' && WEBKIT_CHANNEL_PATTERN.test(k));
    });

    // Time array contains timestamp on each record and what channels are active
    channelStats.time.push({
      timestamp: Date.now(),
      channels: subChannelNames
    });

    // Iterate over "subchannels"
    subChannelNames.forEach(function(subChannelName) {
      if (!(subChannelName in channelStats.stat)) {
        channelStats.stat[subChannelName] = {};
      }

      var src = statItems[subChannelName],
          dest = channelStats.stat[subChannelName];

      for (var item in src) {
        if (Array.isArray(dest[item])) {
          // Just push a value
          dest[item].push(src[item]);
        } else if (src[item] !== dest[item]) {
          if (!(item in dest)) {
            // New property has appeared
            dest[item] = src[item];
          } else {
            // The property already exists in the dest and changed its value - transform value to the array
            dest[item] = new Array(previousSampleCount - _findSubchannelFirstIndex(subChannelName)).map(function() { return dest[item]; });
            dest[item].push(src[item]);
          }
        }
        // Do nothing if src and dest values are the same
      }
    });

    function _findSubchannelFirstIndex(name) {
      for (var i = 0; i < channelStats.time.length; i++) {
        if (channelStats.time[i].channels.indexOf(name) !== -1) {
          return i;
        }
      }
      return 0;
    }
  }

  done();
}
