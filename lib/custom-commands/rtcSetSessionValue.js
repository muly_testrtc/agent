var util = require('util');
var events = require('events');
var request = require('request');

function SetSessionInfo() {
  events.EventEmitter.call(this);
}

util.inherits(SetSessionInfo, events.EventEmitter);

SetSessionInfo.prototype.command = function(varName, varValue, cb) {
  var self = this;

  if (!varName) {
    //self.client.assertion(false, null, null, 'Successfully injected hidden video with ID: #' + FAKE_VIDEO_ID, true);
    throw new Error(util.format('rtcSetSessionValue, name cannot be empty [%s]', varName));
  }
  if (!varValue) {
    throw new Error(util.format('rtcSetSessionValue(%s), value cannot be empty [%s]', varName, varValue));
  }

  console.info(util.format('TEST-RTC-MESSAGE: %s', JSON.stringify({
    msgType: 'session-value',
    varName: varName,
    varValue: varValue
  })));

  process.nextTick(function() {
    self.emit('complete');
  });

  return this;
};

module.exports = SetSessionInfo;
