var util = require('util');
var events = require('events');
var fs = require('fs');
var path = require('path');
var os = require('os');
var rtcTestInfo = require('../../test-utils/rtcTestInfo');
var rtcScreenshot = require('./rtcScreenshot');

function testRTCEnd() {
  events.EventEmitter.call(this);
}

util.inherits(testRTCEnd, events.EventEmitter);

testRTCEnd.prototype.command = function(failed) {

  function writeLogFile(logEntriesArray) {
    console.log('testRTCEnd - Write browser logs', logEntriesArray.length);
    var fileName = process.env.TEST_RESULTS_PATH + '/browser_logs.json';
    fs.writeFileSync(fileName, JSON.stringify(logEntriesArray));
    console.log('Write logs to', fileName);
  }

  var self = this;
  var fileCount = 0;
  var logs = [];

  if (failed) {
    console.log('testRTCEnd - test failed. Taking automatic failure screenshot...');
    self.client.api.rtcScreenshot(rtcScreenshot.failureScreenName);
  }

  self.client.api
    .getLog('browser', function(logEntriesArray) {
      //Split as need to fill logs before execute this part
      writeLogFile(logEntriesArray);

      self.client.api
        .retriveGetStat() // Won't do anything in case getStats is disabled
        .window_handles(function (result) {
          var handle = result.value[0];

          self.client.api
            .switchWindow(handle)
            .element('css selector', 'summary', function(result) {
              if (result.status === -1) {
                return failedToCollectStatistic();
              }

              // Click on summary first
              self.client.api
                .click('summary')
                .waitForElementVisible('a', 2000, false, function(result) {
                  if (!result.value) {
                    return failedToCollectStatistic();
                  }

                  // Click a button and then save
                  self.client.api.click('a', function(result) {
                    if (result.status !== 0) {
                      self.client.api.rtcScreenshot('system-failure', function() {
                        writeScreenshotsFile(self.client.api.rtcScreenshot.files);
                        return failedToCollectStatistic();
                      });
                    }

                    self.saveWebRTCInternals();
                  });

                });
            });
        });
    });

  function failedToCollectStatistic() {
    rtcTestInfo.addMessage('TEST-RTC-WARN', 'warn', 'Unable to collect WebRTC statistic using chrome://webrtc-internals');
    return self.emit('complete');
  }

  return this;
};

testRTCEnd.prototype.saveWebRTCInternals = function() {
  var self = this;

  self.client.api
    .pause(2000, function () {
      console.log('testRTCEnd - Start check for dump file');
      self.startTimeInMilliseconds = new Date().getTime();
      self.check(function () {
        self.emit('complete');
      });
    });
};

testRTCEnd.prototype.check = function(cb) {
  function getUserHome() {
    var home = process.env.HOME || process.env.HOMEPATH || process.env.USERPROFILE;
    if (home === '/root') {
      home = '/home/seluser';
    }

    return home;
  }

  var self = this;
  var maxTimeInMilliseconds = 15000;

  var isWindows = os.platform().indexOf('win') === 0;
  var webRTC_dir = path.resolve((isWindows ? 'C:/Users/Muly' : getUserHome()) + '/Downloads/');
  var webRTC_fileName = 'webrtc_internals_dump.txt'; //.txt';
  var rtcFile = path.join(webRTC_dir, webRTC_fileName);

  var now = new Date().getTime();
  if (fs.existsSync(rtcFile)) {
    console.log('Found webRTC dump file',rtcFile);
    cb();
  } else if (now - self.startTimeInMilliseconds < maxTimeInMilliseconds) {
    console.log('Not found webRTC dump file, set timeout',rtcFile);
    setTimeout(function () {
      self.check(cb);
    }, 1000 /*TestConstants.TIMEOUT_RETRY_INTERVAL*/);
  } else {
    console.log('Not found webRTC dump file, expired',rtcFile);
    cb();
  }
};

module.exports = testRTCEnd;