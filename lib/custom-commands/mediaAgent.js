var util = require('util');
var events = require('events');

function MediaAgent() {
  events.EventEmitter.call(this);
}

util.inherits(MediaAgent, events.EventEmitter);

MediaAgent.prototype.command = function(command, postParams, cb) {
  var self = this;

  console.log('MediaAgent', command, postParams, typeof cb);

  this.client.api.executeAsync(function() {
    function load(url, postParams, callback) {
      var xhr;


      console.log('testRTC:', 'Inside load function', url, postParams);
      if (typeof XMLHttpRequest !== 'undefined') xhr = new XMLHttpRequest();
      else {
        var versions = ["MSXML2.XmlHttp.5.0",
          "MSXML2.XmlHttp.4.0",
          "MSXML2.XmlHttp.3.0",
          "MSXML2.XmlHttp.2.0",
          "Microsoft.XmlHttp"]

        for (var i = 0, len = versions.length; i < len; i++) {
          try {
            xhr = new ActiveXObject(versions[i]);
            break;
          }
          catch (e) {
          }
        } // end for
      }

      xhr.onreadystatechange = ensureReadiness;

      function ensureReadiness() {
        if (xhr.readyState < 4) {
          return;
        }

        if (xhr.status !== 200) {
          return;
        }

        // all is well
        if (xhr.readyState === 4) {
          callback(xhr);
        }
      }

      if (postParams) {
        xhr.open('POST', url, true);
        xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhr.send(JSON.stringify(postParams));
      }
      else {
        xhr.open('GET', url, true);
        xhr.send('');
      }
    }

    var cb = arguments[arguments.length - 1];
    console.log('testRTC:', 'Request ', arguments[0], arguments[1]);
    load(arguments[0], arguments[1], function (xhr) {
      console.log('testRTC: Agent Response <- ', xhr.responseText);
      cb(xhr.responseText);
    })
  },
  [command, postParams, cb],
  function (result) {
    console.log('mediaAgent: ExecuteAsync', result.value);
    if (typeof(cb) == 'function') {
      cb.call(self.client.api, result.value);
    }
    self.emit('complete');
  });

  return this;
};

module.exports = MediaAgent;