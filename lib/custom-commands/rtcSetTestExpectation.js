var util = require('util');
var events = require('events');
var request = require('request');
var rtcTestInfo = require('../../test-utils/rtcTestInfo');

function SetTestExpectation() {
  events.EventEmitter.call(this);
}

util.inherits(SetTestExpectation, events.EventEmitter);

SetTestExpectation.prototype.command = function(expression, message, alertType) {
  var self = this;

  rtcTestInfo.addValue('expectation', 'expectation', {expression: expression, msg: message, alert: alertType});
  process.nextTick(function() {
    self.emit('complete');
  });
  return this;
};

module.exports = SetTestExpectation;
