var util = require('util');
var events = require('events');
var spawn = require('child_process').spawn;
var path = require('path');
var fs = require('fs');

var acceptSaveDialogCommand = {
	'darwin': [
    'osascript',
    [ '-e', 'tell application "System Events"\ntell application "Google Chrome" to activate\ndelay 1\nkey code 36\ndelay 1\nset visible of process "Google Chrome" to false\nend tell\n' ]
  ],
	'linux': [
    'xdotool',
    //['key', '--window', '$(xdotool search --class Chrome | head -n 1)', 'Return']
    ['key', 'Return']
    //['key', 'alt+s']
  ]
};

// Do not interrupt in case we haven't been able to start recording
var INTERRUPT_ON_ERROR = false;

function RTCStartAudioRecording() {
  events.EventEmitter.call(this);
}

util.inherits(RTCStartAudioRecording, events.EventEmitter);

RTCStartAudioRecording.prototype.command = function(url) {
  var self = this,
      api = this.api;

  api.window_handles(function(result) {
    if (!Array.isArray(result.value) || result.value.length < 2) {
      self.client.assertion(false, null, null, 'Can\'t access webrtc-internals' , INTERRUPT_ON_ERROR);
      return self.emit('complete');
    }

    var webrtcInternalsHandle = result.value[0],
        mainWindowHandle = result.value[1];

    api
      .switchWindow(webrtcInternalsHandle)
      .perform(function(client, done) {
        self.tryCloseDialogAsync(function (err) {
          // Switch tab back
          api
            .info('Supposed to close the dialog')
            .switchWindow(mainWindowHandle);

          if (err) {
            console.log('RTCStartAudioRecording error closing Save As dialog:', err);
            self.client.assertion(false, null, null, 'Can\'t start audio recording using webrtc-internals', INTERRUPT_ON_ERROR);
            return self.emit('complete');
          }

          self.client.assertion(true, true, true, 'Audio recording has been started', false);
          self.emit('complete');
        });

        // Move immediately to the next .execute()
        done();
      })
      .execute(function() {
        // This will open Save As dialog
        chrome.send('enableAudioDebugRecordings');
      });
    });

  return this;
};

RTCStartAudioRecording.prototype.tryCloseDialogAsync = function(cb) {
  var retries = 0,
      args = acceptSaveDialogCommand[process.platform];

  setTimeout(function() {
    spawn
      .apply(null, args)
      .on('close', function(code) {
        console.log('RTCStartAudioRecording: acceptSaveDialogCommand finished with code:', code);
        // Here we are expecting the window to be closed
        cb(null);
      });
  }, 1000);
};

module.exports = RTCStartAudioRecording;
