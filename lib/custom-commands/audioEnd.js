var util = require('util');
var events = require('events');

function AudioEnd() {
  events.EventEmitter.call(this);
}

util.inherits(AudioEnd, events.EventEmitter);

AudioEnd.prototype.command = function(cb) {
  var self = this;

  this.client.api.mediaAgent('audio-end', null, function(result) {
    //console.log('AudioEnd ->', result);
    cb.call(self.client.api, result);
    self.emit('complete');
  });

  return this;
};

module.exports = AudioEnd;