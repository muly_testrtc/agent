var util = require('util');
var events = require('events');
var rtcTestInfo = require('../../test-utils/rtcTestInfo');

function rtcGetTimer() {
  events.EventEmitter.call(this);
}

util.inherits(rtcGetTimer, events.EventEmitter);

rtcGetTimer.prototype.command = function(timerName, callback) {
  var self = this;

  var val = 0;
  var timers = rtcTestInfo.getTimers();
  var timerStart = timers[timerName];
  if (timerStart) {
    var now = (new Date()).getTime();
    val = now - timerStart;
  } else {
    val = 0;
  }

  if (callback) {
    callback.call(self, val);
  }

  process.nextTick(function() {
    self.emit('complete');
  });
  return this;
};

module.exports = rtcGetTimer;