var util = require('util');
var events = require('events');
var request = require('request');
var rtcTestInfo = require('../../test-utils/rtcTestInfo');

function SetMetric() {
  events.EventEmitter.call(this);
}

util.inherits(SetMetric, events.EventEmitter);

SetMetric.prototype.command = function(varName, op, varValue, aggType) {
  var self = this;

  rtcTestInfo.addValue('metric', varName, {op: op, val: varValue, agg: aggType});
  process.nextTick(function() {
    self.emit('complete');
  });
  return this;
};

module.exports = SetMetric;
