var util = require('util');
var events = require('events');
var request = require('request');

function SetSessionInfo() {
  events.EventEmitter.call(this);
}

util.inherits(SetSessionInfo, events.EventEmitter);

SetSessionInfo.prototype.command = function(varName, varValue, cb) {
  var self = this;

  var url = process.env.ADMIN_URL + '/api/agents/room-info/' + process.env.RTC_SESSION_NAME + '/' + varName + '/';
  console.log(util.format('SetSessionInfo -> varName:[%s], varValue:[%s], url:[%s]', varName, varValue, url));

  if (!varName) {
    //self.client.assertion(false, null, null, 'Successfully injected hidden video with ID: #' + FAKE_VIDEO_ID, true);
    throw new Error(util.format('rtcSetSessionValue, name cannot be empty [%s]', varName));
  }
  if (!varValue) {
    throw new Error(util.format('rtcSetSessionValue(%s), value cannot be empty [%s]', varName, varValue));
  }

  //self.client.assertion(!!varName, null, null, 'rtcSetSessionValue, name cannot be empty', true);
  //self.client.assertion(!!varValue, null, null, 'rtcSetSessionValue, value cannot be empty', true);


  request({
    method: 'POST',
    strictSSL: false,
    url: url,
    body: {
      value: varValue
    },
    json: true
  }, function (error, response, body) {
    console.log('SetSessionInfo <-', error, body);
    if (cb) {
      cb.call(self.client.api, body);
    }
    self.emit('complete');
  });


  /*
  this.client.api.mediaAgent(url, { varValue: varValue}, function(result) {
    console.log('SetSessionInfo <-', result);
    if (cb) {
      cb.call(self.client.api, result);
    }
    self.emit('complete');
  });
  */

  return this;
};

module.exports = SetSessionInfo;
