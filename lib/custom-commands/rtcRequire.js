var util = require('util');
var events = require('events');
var request = require('request');

function rtcRequire() {
  events.EventEmitter.call(this);
}

util.inherits(rtcRequire, events.EventEmitter);

rtcRequire.prototype.command = function(url, cb) {
  var self = this;

  function requireFromString(src, filename) {
    var Module = module.constructor;
    var m = new Module();
    m._compile(src, filename);
    return m.exports;
  }

  request({
    method: 'GET',
    strictSSL: false,
    url: url,
  }, function (error, response, body) {
    var code = body.toString();
    console.log('rtcRequire code:', code);
    var module = requireFromString(code);

    if (cb) {
      cb.call(self.client.api, module);
    }
    self.emit('complete');
  });

  return this;
};

module.exports = rtcRequire;