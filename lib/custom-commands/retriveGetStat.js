var util = require('util');
var events = require('events');
var fs = require('fs');
var rtcTestInfo = require('../../test-utils/rtcTestInfo');

function retriveGetStat() {
  events.EventEmitter.call(this);
}

util.inherits(retriveGetStat, events.EventEmitter);

retriveGetStat.prototype.command = function(cb) {
  var self = this;

  var useGetStat = rtcTestInfo.getTestOptions().useGetStat;

  if (!rtcTestInfo.getTestOptions().useGetStat) {
    // getStats is disabled
    process.nextTick(function() {
      self.emit('complete');
    });
    return this;
  }

  this.client.api.executeAsync(function() {
      var answer = ___testRTCTestAgent.stats;
      var cb = arguments[arguments.length - 1];
      cb(answer);
  },
  [cb],
  function (result) {
    var getStatData = {};
    if (result && !result.status && result.value) {
      // Data retrieved successfully
      getStatData = result.value; 
    }

    console.log('retriveGetStat - Write getStat object', getStatData);
    var fileName = process.env.TEST_RESULTS_PATH + '/getstat_logs.json';
    fs.writeFileSync(fileName, JSON.stringify(getStatData));
    console.log('Write logs to', fileName);
    
    if (typeof(cb) == 'function') {
      cb.call(self.client.api, getStatData);
    }
    self.emit('complete');
  });

  return this;
};

module.exports = retriveGetStat;