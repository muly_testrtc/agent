#replace SSH keys for machine

#https://azure.microsoft.com/en-us/documentation/articles/virtual-machines-linux-use-vmaccess-reset-password-or-ssh/#sshkeyresetcli
#using PS

$CSName = "jankins"
$VMName = "jankins"
$vm = Get-AzureVM -ServiceName $CSName -Name $VMName

$vm.GetInstance().ProvisionGuestAgent = $true

$UserName = "ubuntu"
$Cert = Get-Content "./cert/id_rsa.pub"
$PrivateConfig = '{"username":"' + $UserName + '", "ssh_key":"' + $cert + '"}'
$ExtensionName = "VMAccessForLinux"
$Publisher = "Microsoft.OSTCExtensions"
$Version =  "1.*"
Set-AzureVMExtension -ExtensionName $ExtensionName -VM  $vm -Publisher $Publisher -Version $Version -PrivateConfiguration $PrivateConfig | Update-AzureVM


#Connect to each machine
ssh ubuntu@mongo-staging.cloudapp.net -p 2200 -i cert/id_rsa

#admin mongo (from cloud.mms)
sudo /opt/mongodb-mms-automation/bin/mongodb-mms-automation-make-init-scripts -h

$PrivateConfig = '{"reset_ssh": "True"}'
$ExtensionName = "VMAccessForLinux"
$Publisher = "Microsoft.OSTCExtensions"
$Version = "1.*"
Set-AzureVMExtension -ExtensionName $ExtensionName -VM  $vm -Publisher $Publisher -Version $Version -PrivateConfiguration $PrivateConfig | Update-AzureVM