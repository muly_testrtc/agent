Split project

# Muly
Start selenium so I can see the logs of it
java -Dwebdriver.chrome.driver=bin/chromedriver.exe -jar ./bin/selenium-server-standalone-2.44.0.jar


Linux
java -Dwebdriver.chrome.driver=bin/chromedriver -jar ./bin/selenium-server-standalone-2.44.0.jar


Modify nightwatch.json
  "selenium" : {
    "start_process" : false,

Start the test
./bin/nightwatch.js --env chrome,chrome


Satrt APIDaze test
chmod +x bin/nightwatch.js
./bin/nightwatch.js --env chrome --test tests/callAPIdaze


Test Chrome

"C:\Users\Muly\AppData\Local\Google\Chrome SxS\Application\chrome.exe" --use-fake-device-for-media-stream --use-fake-ui-for-media-stream --use-file-for-fake-audio-capture=./sound/a2002011001-e02-8kHz.wav http://googlechrome.github.io/webrtc/samples/web/content/getusermedia/audio/

"C:\Users\Muly\AppData\Local\Google\Chrome SxS\Application\chrome.exe" --version



# Nightwatch WebRTC Demo

Demo test project for [Testing WebRTC apps with Nightwatch](http://nightwatchjs.org/blog/testing-webrtc-apps-with-nightwatch/) blog article.

//"verbose", "log-path=chromedriver.log", "enable-logging", "v=4", 

***

#### [Homepage](http://nightwatchjs.org) | [Developer Guide](http://nightwatchjs.org/guide) | [API Reference](http://nightwatchjs.org/api)

### Running the test

1) Install [Node.js](http://nodejs.org) if not present and clone the repo:
```sh
$ git clone https://github.com/beatfactor/nightwatch-webrtcdemo.git
$ cd nightwatch-webrtcdemo
$ npm install
```

2) Download the `selenium-server-standalone-2.43.1.jar` or newer from http://selenium-release.storage.googleapis.com/index.html and place it in your `bin` folder - remember to update `nightwatch.json` if you download a newer version.

3) Download the `chromedriver` from http://chromedriver.storage.googleapis.com/index.html and place it in your `bin` folder

4) To run the test with 2 chrome peers
```sh
$ ./bin/nightwatch.js --env chrome,chrome
```


Test sync
