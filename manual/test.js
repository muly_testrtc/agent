var fs = require('fs');

module.exports = new (function() {
  var testCases = this;

  testCases['get random image'] = function (client) {
    console.log('env.TEST_RESULTS_PATH', process.env.TEST_RESULTS_PATH);
    client
      .info('Start test')
      .timeoutsAsyncScript(15000)
      .url('http://85fc8ccd760a621e9d81-e9638d787a6e32e99181ab75ac8c3328.r68.cf2.rackcdn.com/test_page.html')
      .info('Open URL')
      .waitForElementVisible('body', 35000)
      .waitForElementVisible('#download-sample', 35000)
      .takeScreenshot('screen1')
      .info('Click')
      .click("#download-sample")
      .pause(2000) // need to wait to file to finish download
      .info('After Wait')
      .getLog('browser', function(logEntriesArray) {
        var fileName = process.env.TEST_RESULTS_PATH + '/browser_logs.json';
        fs.writeFileSync(fileName, JSON.stringify(logEntriesArray));
        console.log('Write logs to', fileName, logEntriesArray);
      })
      .end();
  };
})();