/**
 * This is universal version of client-side content script
 *  for use in both Chrome and Firefox
 */

(function(window) {
  window.___testRTCExtension = {
    webkitRTCPeerConnection: window["webkitRTCPeerConnection"],
    webkitGetStats: window["webkitRTCPeerConnection"] && window["webkitRTCPeerConnection"].prototype.getStats
  };

  window.___testRTCTestAgent = {
    nextChannelId: 0,
    knownChannels: {},
    stats: {
      /*
        Hash containing parsed getStats() data for each connection
        Key is RTCPeerConnection.___testRTCChannelId
        Values are set in injectGetStat.js
      */
    },
    extra: {
      /*
        Hash containing extra data per each connection
        Key is RTCPeerConnection.___testRTCChannelId
      */
    }
  };

  function onIceConnectionStateChange() {
    var self = this;
    switch(this.iceConnectionState) {
    case 'connected':
    case 'completed':
      this.removeEventListener('iceconnectionstatechange', arguments.callee);
      this.getStats(function(response) {
        var reports = response.result();
        var stats = {};
        reports.forEach(function(report) {
          stats[report.id] = {
            id: report.id,
            type: report.type
          };
          report.names().forEach(function(name) {
            stats[report.id][name] = report.stat(name);
          });
        });
        Object.keys(stats).forEach(function(id) {
          var report = stats[id];
          if (report.type === 'googCandidatePair' && report.googActiveConnection === 'true') {
            var localCand = stats[report.localCandidateId];
            //var remoteCand = stats[report.remoteCandidateId];
            window.___testRTCTestAgent.extra[self.___testRTCChannelId].localCand = localCand;
            // contains information on ip addresses and more importantly priority.
            // priority >> 24 gives TURN type (udp=2/tcp=1/tls=0)
          }
        });
      }, null);
    }
  }

  function getSDP() {
    switch(this.iceConnectionState) {
    case 'connected':
    case 'completed':
    case 'failed':
      this.removeEventListener('iceconnectionstatechange', arguments.callee);
      // inspect for number of candidates etc.
      window.___testRTCTestAgent.extra[this.___testRTCChannelId].localDescription = this.localDescription;
      window.___testRTCTestAgent.extra[this.___testRTCChannelId].remoteDescription = this.remoteDescription;
    }
  }

  var originalRTCPeerConnection = window.___testRTCExtension.webkitRTCPeerConnection,
      originalRTCPeerConnectionName = "webkitRTCPeerConnection";

  var originalRTCPeerConnectionClose = originalRTCPeerConnection.prototype.close;
  originalRTCPeerConnection.prototype.close = function() {
    delete window.___testRTCTestAgent.knownChannels[this.___testRTCChannelId];
    return originalRTCPeerConnectionClose.apply(this, arguments);
  };

  window[originalRTCPeerConnectionName] = function(pcConfig, pcConstraints) {
    var conn = new originalRTCPeerConnection(pcConfig, pcConstraints);
    conn.___testRTCChannelId = window.___testRTCTestAgent.nextChannelId;
    window.___testRTCTestAgent.knownChannels[conn.___testRTCChannelId] = conn; 
    window.___testRTCTestAgent.nextChannelId++;
    window.___testRTCTestAgent.extra[conn.___testRTCChannelId] = {};
    conn.addEventListener('iceconnectionstatechange', onIceConnectionStateChange.bind(conn));
    conn.addEventListener('iceconnectionstatechange', getSDP.bind(conn));
    return conn;
  };

  window[originalRTCPeerConnectionName].prototype = originalRTCPeerConnection.prototype;


  window.addEventListener('message', function(evt) {
    if (evt.source === window && evt.data.type === 'TESTRTC_OPTIONS' && evt.data.options) {
      window.removeEventListener('message', arguments.callee);
      if (evt.data.options.useGetStat) {
        injectGetStatInterval();
      }
    }
  });

  function injectGetStatInterval() {
    setInterval(function() {
      for (var channelId in ___testRTCTestAgent.knownChannels) {
        // Iterate over active connections
        var conn = ___testRTCTestAgent.knownChannels[channelId];
        if (!conn) {
          continue;
        }

        if (___testRTCExtension['mozRTCPeerConnection']) {
          mozGetStats(conn, channelId);
        } else if (___testRTCExtension['webkitRTCPeerConnection']) {
          webkitGetStats(conn, channelId);
        }
      }
    }, 1000);

    function webkitGetStats(conn, channelId) {
      var getStats = ___testRTCExtension.webkitGetStats.bind(conn);
      getStats(function(response) {
        /**
         * This is a raw version of Chrome getStats
         * TODO: this should be changed once https://bugs.chromium.org/p/webrtc/issues/detail?id=2031 is fixed
         */
        var standardReport = {};
        var reports = response.result();
        reports.forEach(function(report) {
          var standardStats = {
            id: report.id,
            timestamp: report.timestamp,
            type: report.type
          };
          report.names().forEach(function(name) {
            standardStats[name] = report.stat(name);
          });
          standardReport[standardStats.id] = standardStats;
        });

        appendStats(conn, channelId, standardReport, 'webkit');
      });
    }

    function mozGetStats(conn, channelId) {
      var getStats = ___testRTCExtension.mozGetStats.bind(conn);

      getStats(null, function(results) {
        appendStats(conn, channelId, results, 'firefox');
      }, function(err) {
        // Do nothing on error
      });
    }

    var WEBKIT_CHANNEL_PATTERN = /^(ssrc_\w+_(recv|send))|(Conn-(data|audio|video)-\w+.*)$/i,
        FIREFOX_CHANNEL_PATTERN = /^(in|out)bound_rtp/;

    /**
     * This function handles raw GetStats data and consolidates it
     *  in order to keep stat file small
     */
    function appendStats(conn, channelId, statItems, client) {
      if (!(channelId in ___testRTCTestAgent.stats)) {
        var extra = ___testRTCTestAgent.extra[channelId] || {};

        // The first record for this channelId
        ___testRTCTestAgent.stats[channelId] = {
          channelId: channelId,
          time: [],
          stat: {},
          extra: extra, // TODO: move to retrieveGetStat.js
          client: client
        };
      }

      // Shortcut reference
      var channelStats = ___testRTCTestAgent.stats[channelId];
      // How many samples have been recorded for the current channel
      var previousSampleCount = channelStats.time.length;

      // Get "subchannel" names
      var subChannelNames = Object.keys(statItems).filter(function(k) {
        if (!statItems.hasOwnProperty(k) || typeof statItems[k] !== 'object') {
          return false;
        }
        
        return (client === 'firefox' && FIREFOX_CHANNEL_PATTERN.test(k)) || (client === 'webkit' && WEBKIT_CHANNEL_PATTERN.test(k));
      });

      // Time array contains timestamp on each record and what channels are active
      channelStats.time.push({
        timestamp: Date.now(),
        channels: subChannelNames
      });

      // Iterate over "subchannels"
      subChannelNames.forEach(function(subChannelName) {
        if (!(subChannelName in channelStats.stat)) {
          channelStats.stat[subChannelName] = {};
        }

        var src = statItems[subChannelName],
            dest = channelStats.stat[subChannelName];

        for (var item in src) {
          if (Array.isArray(dest[item])) {
            // Just push a value
            dest[item].push(src[item]);
          } else if (src[item] !== dest[item]) {
            if (!(item in dest)) {
              // New property has appeared
              dest[item] = src[item];
            } else {
              // The property already exists in the dest and changed its value - transform value to the array
              dest[item] = new Array(previousSampleCount - _findSubchannelFirstIndex(subChannelName)).map(function() { return dest[item]; });
              dest[item].push(src[item]);
            }
          }
          // Do nothing if src and dest values are the same
        }
      });

      function _findSubchannelFirstIndex(name) {
        for (var i = 0; i < channelStats.time.length; i++) {
          if (channelStats.time[i].channels.indexOf(name) !== -1) {
            return i;
          }
        }
        return 0;
      }
    }
  }

})(window);
