chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
    if (request.type === 'TESTRTC_SET_OPTIONS') {
        chrome.storage.sync.set({ 'TESTRTC_OPTIONS': request.options });
    } else if (request.type === 'TESTRTC_GET_OPTIONS') {
        var options = null;
        chrome.storage.sync.get('TESTRTC_OPTIONS', function(items) {
            options = items['TESTRTC_OPTIONS'] || null;

            sendResponse({ options: options });
        });
    }

    return true;
});
