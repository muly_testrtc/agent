/**
* Read TESTRTC_OPTIONS from the query string and save options to extension storage
* This message comes to us from blank page (see testRTCStart command)
*/
if (window.location.protocol === 'file:' && window.location.search.indexOf('TESTRTC_OPTIONS') !== -1) {
  // We've got options. Do not inject script, just save options to extension storage

  var options = null,
      tokens = window.location.search.slice(1).split('=');

  try {
    options = JSON.parse(decodeURIComponent(tokens[tokens.indexOf('TESTRTC_OPTIONS') + 1]));
  } catch (e) {}

  chrome.runtime.sendMessage({ type: 'TESTRTC_SET_OPTIONS', options: options });
} else {
  // Otherwise inject script and send options there

  var s = document.createElement('script');
  s.src = chrome.extension.getURL('inject.js');
  s.onload = function() {
    this.parentNode.removeChild(this);

    /**
     * Request options from the storage and send back to the each page
     */
    chrome.runtime.sendMessage({ type: 'TESTRTC_GET_OPTIONS' }, function(response) {
      if (response.options) {
        window.postMessage({ type: 'TESTRTC_OPTIONS', options: response.options }, '*');
      }
    });
  };
  (document.head || document.documentElement).appendChild(s);
}
