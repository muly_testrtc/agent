/**
 * This is Firefox version of TestRTC extension
 */

var ___testRTCInit = function(window) {
  var originalRTCPeerConnectionName = window.RTCPeerConnection ? "RTCPeerConnection" : "mozRTCPeerConnection",
      originalRTCPeerConnection = window[originalRTCPeerConnectionName];

  if (!originalRTCPeerConnection) {
    console.error("_testRTC_", "TestRTC API has not been injected: WebRTC API is unavailable");
    return;
  }

  window.___testRTCExtension = {
    mozRTCPeerConnection: window[originalRTCPeerConnectionName],
    mozGetStats: window[originalRTCPeerConnectionName] && window[originalRTCPeerConnectionName].prototype.getStats,

    getUserMediaSuccessCallbacks: null,
    onGetUserMedia: null
  };

  window.___testRTCTestAgent = {
    nextChannelId: 0,
    knownChannels: {},
    stats: {
      /*
        Hash containing parsed getStats() data for each connection
        Key is RTCPeerConnection.___testRTCChannelId
        Values are set in injectGetStat.js
      */
    },
    extra: {
      /*
        Hash containing extra data per each connection
        Key is RTCPeerConnection.___testRTCChannelId
      */
    }
  };

  function onIceConnectionStateChange() {
    var self = this;
    switch(this.iceConnectionState) {
    case 'connected':
    case 'completed':
      this.removeEventListener('iceconnectionstatechange', arguments.callee);
      this.getStats(null, function(stats) {
        Object.keys(stats).forEach(function(id) {
          var report = stats[id];
          if (report.type === 'candidatepair' && report.selected === true) {
            //var remoteCand = stats[report.remoteCandidateId];
            // contains information on ip addresses and more importantly priority.
            // priority >> 24 gives TURN type (udp=5/tcp=0)
            var localCand = stats[report.localCandidateId];
            // well... not: https://bugzilla.mozilla.org/show_bug.cgi?id=1184197
            // so we look in the local description for that candidate...
            self.localDescription.sdp.split('\r\n').forEach(function(line) {
              if (line.indexOf('a=candidate:') === 0) {
                line = line.split(' ');
                if (line[4] === localCand.ipAddress
                    && line[5] === localCand.portNumber.toString()) {
                  localCand.priority = parseInt(line[3], 10);
                  window.___testRTCTestAgent.extra[self.___testRTCChannelId].localCand = localCand;
                }
              }
            });
          }
        });
      }, function(err) {
        // do nothing.
      });
    }
  }

  function getSDP() {
    switch(this.iceConnectionState) {
    case 'connected':
    case 'completed':
    case 'failed':
      this.removeEventListener('iceconnectionstatechange', arguments.callee);
      // inspect for number of candidates etc.
      window.___testRTCTestAgent.extra[this.___testRTCChannelId].localDescription = this.localDescription;
      window.___testRTCTestAgent.extra[this.___testRTCChannelId].remoteDescription = this.remoteDescription;
    }
  }

  var originalRTCPeerConnectionClose = originalRTCPeerConnection.prototype.close;
  originalRTCPeerConnection.prototype.close = function() {
    delete window.___testRTCTestAgent.knownChannels[this.___testRTCChannelId];
    return originalRTCPeerConnectionClose.apply(this, arguments);
  };

  window[originalRTCPeerConnectionName] = function(pcConfig, pcConstraints) {
    var conn = new originalRTCPeerConnection(pcConfig, pcConstraints);
    conn.___testRTCChannelId = window.___testRTCTestAgent.nextChannelId;
    window.___testRTCTestAgent.knownChannels[conn.___testRTCChannelId] = conn;
    window.___testRTCTestAgent.nextChannelId++; 
    window.___testRTCTestAgent.extra[conn.___testRTCChannelId] = {};
    conn.addEventListener('iceconnectionstatechange', onIceConnectionStateChange.bind(conn));
    conn.addEventListener('iceconnectionstatechange', getSDP.bind(conn));
    return conn;
  };

  window[originalRTCPeerConnectionName].prototype = originalRTCPeerConnection.prototype;

  // End of RTCPeerConnection injection
  // Injecting getUserMedia for the firefox

  if (HTMLVideoElement.prototype.mozCaptureStream) {
    // We have mozCaptureStream - let's use it
    window.___testRTCExtension.getUserMediaSuccessCallbacks = [];

    if (window.navigator.mediaDevices) {
      // Override a new-style function which returns Promise
      window.navigator.mediaDevices.getUserMedia = function(constraints) {
        return new Promise(function(resolve, reject) {
          window.___testRTCExtension.getUserMediaSuccessCallbacks.push({
            callback: resolve,
            constraints: constraints
          });
          window.___testRTCExtension.onGetUserMedia && window.___testRTCExtension.onGetUserMedia();
        });
      };
    }

    // Override old-style function too
    window.navigator.getUserMedia = window.navigator.mozGetUserMedia = function(constraints, successCallback, errorCallback) {
      window.___testRTCExtension.getUserMediaSuccessCallbacks.push({
        callback: resolve,
        constraints: constraints
      });
      window.___testRTCExtension.onGetUserMedia && window.___testRTCExtension.onGetUserMedia();
    };
  }

  window.addEventListener('message', function(evt) {
    if (evt.data.type === 'TESTRTC_OPTIONS') {
      preloadFakeVideo(evt.data.options);
    }
  }, this);

  injectGetStatInterval();

  function preloadFakeVideo(options) {
    if (!options || !options.videoURL) {
      throw new Error('Error injecting TestRTC API: video URL is not available');
    }

    var xmlHTTP = new XMLHttpRequest();
    xmlHTTP.open('GET', options.videoURL, true);
    xmlHTTP.responseType = 'blob';
    xmlHTTP.onload = function() {
      injectFakeGetUserMedia(xmlHTTP.response, options.videoMimeType);
    };
    xmlHTTP.send();
  }

  function injectFakeGetUserMedia(blob, mimeType) {
    var videoID = 'fakeVideo1';
    var fakeVideo = document.createElement('video');
    fakeVideo.id = videoID; 
    fakeVideo.width = 160;
    fakeVideo.height = 120;
    fakeVideo.loop = true;
    fakeVideo.preload = 'auto';
    fakeVideo.style.display = 'none';
    
    var fakeSource = document.createElement('source');
    fakeSource.type = mimeType;
    fakeSource.src = URL.createObjectURL(blob);
    fakeVideo.appendChild(fakeSource);
    document.body.appendChild(fakeVideo);

    var fnTriggerGetUserMediaCallbacks = function() {
      var stream = document.getElementById(videoID).mozCaptureStream();
      while (___testRTCExtension.getUserMediaSuccessCallbacks.length) {
        ___testRTCExtension.getUserMediaSuccessCallbacks.pop().call(null, stream);
        document.getElementById(videoID).play();
      }
    };

    // Set a callback in order to handle future requests to getUserMedia
    ___testRTCExtension.onGetUserMedia = fnTriggerGetUserMediaCallbacks;
    fnTriggerGetUserMediaCallbacks();
  }

  function injectGetStatInterval() {
    setInterval(function() {
      for (var channelId in ___testRTCTestAgent.knownChannels) {
        // Iterate over active connections
        var conn = ___testRTCTestAgent.knownChannels[channelId];
        if (!conn) {
          continue;
        }

        if (___testRTCExtension['mozRTCPeerConnection']) {
          mozGetStats(conn, channelId);
        } else if (___testRTCExtension['webkitRTCPeerConnection']) {
          webkitGetStats(conn, channelId);
        }
      }
    }, 1000);

    function webkitGetStats(conn, channelId) {
      var getStats = ___testRTCExtension.webkitGetStats.bind(conn);
      getStats(function(response) {
        /**
         * This is a raw version of Chrome getStats
         * TODO: this should be changed once https://bugs.chromium.org/p/webrtc/issues/detail?id=2031 is fixed
         */
        var standardReport = {};
        var reports = response.result();
        reports.forEach(function(report) {
          var standardStats = {
            id: report.id,
            timestamp: report.timestamp,
            type: report.type
          };
          report.names().forEach(function(name) {
            standardStats[name] = report.stat(name);
          });
          standardReport[standardStats.id] = standardStats;
        });

        appendStats(conn, channelId, standardReport, 'webkit');
      });
    }

    function mozGetStats(conn, channelId) {
      var getStats = ___testRTCExtension.mozGetStats.bind(conn);

      getStats(null, function(results) {
        appendStats(conn, channelId, results, 'firefox');
      }, function(err) {
        // Do nothing on error
      });
    }

    var WEBKIT_CHANNEL_PATTERN = /^(ssrc_\w+_(recv|send))|(Conn-(data|audio|video)-\w+.*)$/i,
        FIREFOX_CHANNEL_PATTERN = /^(in|out)bound_rtp/;

    /**
     * This function handles raw GetStats data and consolidates it
     *  in order to keep stat file small
     */
    function appendStats(conn, channelId, statItems, client) {
      if (!(channelId in ___testRTCTestAgent.stats)) {
        var extra = ___testRTCTestAgent.extra[channelId] || {};

        // The first record for this channelId
        ___testRTCTestAgent.stats[channelId] = {
          channelId: channelId,
          time: [],
          stat: {},
          extra: extra, // TODO: move to retrieveGetStat.js
          client: client
        };
      }

      // Shortcut reference
      var channelStats = ___testRTCTestAgent.stats[channelId];
      // How many samples have been recorded for the current channel
      var previousSampleCount = channelStats.time.length;

      // Get "subchannel" names
      var subChannelNames = Object.keys(statItems).filter(function(k) {
        if (!statItems.hasOwnProperty(k) || typeof statItems[k] !== 'object') {
          return false;
        }
        
        return (client === 'firefox' && FIREFOX_CHANNEL_PATTERN.test(k)) || (client === 'webkit' && WEBKIT_CHANNEL_PATTERN.test(k));
      });

      // Time array contains timestamp on each record and what channels are active
      channelStats.time.push({
        timestamp: Date.now(),
        channels: subChannelNames
      });

      // Iterate over "subchannels"
      subChannelNames.forEach(function(subChannelName) {
        if (!(subChannelName in channelStats.stat)) {
          channelStats.stat[subChannelName] = {};
        }

        var src = statItems[subChannelName],
            dest = channelStats.stat[subChannelName];

        for (var item in src) {
          if (Array.isArray(dest[item])) {
            // Just push a value
            dest[item].push(src[item]);
          } else if (src[item] !== dest[item]) {
            if (!(item in dest)) {
              // New property has appeared
              dest[item] = src[item];
            } else {
              // The property already exists in the dest and changed its value - transform value to the array
              dest[item] = new Array(previousSampleCount - _findSubchannelFirstIndex(subChannelName)).map(function() { return dest[item]; });
              dest[item].push(src[item]);
            }
          }
          // Do nothing if src and dest values are the same
        }
      });

      function _findSubchannelFirstIndex(name) {
        for (var i = 0; i < channelStats.time.length; i++) {
          if (channelStats.time[i].channels.indexOf(name) !== -1) {
            return i;
          }
        }
        return 0;
      }
    }
  }
};

/**
 * Inject our function source code into head and call immediately
 * This is needed in order to get access to page's scope directly
 */
 if (window.location.protocol === 'file:' && window.location.search.indexOf('TESTRTC_OPTIONS') !== -1) {
  // For blank page, wait for video URL is available and store in extension storage
  var options = null,
      tokens = window.location.search.slice(1).split('=');

  try {
    options = JSON.parse(decodeURIComponent(tokens[tokens.indexOf('TESTRTC_OPTIONS') + 1]));
    self.port.emit('TESTRTC_SET_OPTIONS', options);
  } catch (e) {}
} else {
  var script = document.createElement('script');
  script.textContent = ___testRTCInit.toSource() + '(window);';
  (document.head||document.documentElement).appendChild(script);
  script.parentNode.removeChild(script);

  self.port.on('TESTRTC_OPTIONS', function(message) {
    if (message) {
      window.postMessage({ type: 'TESTRTC_OPTIONS', options: message }, '*');
    }
  });
  self.port.emit('TESTRTC_GET_OPTIONS');
}
