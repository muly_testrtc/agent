var data = require("sdk/self").data;
var ss = require("sdk/simple-storage");
var pageMod = require("sdk/page-mod");

pageMod.PageMod({
  include: ["*", "file://*"],
  contentScriptFile: data.url("inject.js"),
  contentScriptWhen: 'start',
  onAttach: startListening
});

function startListening(worker) {
    worker.port.on('TESTRTC_SET_OPTIONS', function(message) {
        ss.storage.TESTRTC_OPTIONS = message;
    });
    worker.port.on('TESTRTC_GET_OPTIONS', function(message) {
        var options = ss.storage.TESTRTC_OPTIONS || null;
        worker.port.emit('TESTRTC_OPTIONS', options);
    });
}
