var _ = require('lodash');
var gulp = require('gulp');
var debug = require('gulp-debug');
var template = require('gulp-template');
var data = require('gulp-data');
var foreach = require('gulp-foreach');
var replace = require('gulp-replace');
var chmod = require('gulp-chmod');
var del = require('del');
var merge = require('merge2');
var fs = require('fs');
var gutil = require('gulp-util');

var getJsonData = require('./docker-admin/shell-utils').loadConfig('./docker-data.json');

gulp.task('default', ['build', 'build-sh']);

gulp.task('clean', function (cb) {
  del([
    'dist/**/*',
  ], cb);
});

gulp.task('build', ['clean'], function () {
  var taskList = [];
  var scriptStart = '#! /bin/bash\n\nset -e\n';
  var build = scriptStart;
  var push = scriptStart;
  var pull = scriptStart;
  var buildVersion = scriptStart;


  //docker build -f images/MediaConvert/Dockerfile -t mulyoved/media-convert:v0.6.1 .
  function add(version, name, dockerPath, path, imagename, isPush) {
    if (!gutil.env.chrome || gutil.env.chrome === version) {

      gutil.log('generate', gutil.colors.magenta(version), name);

      if (path) {
        build += '\npushd ' + path;
      }
      build += '\necho "*Build ' + name + ' ' + imagename + '"\ndocker build -f ' + dockerPath + ' -t ' + imagename + ' .';
      if (path) {
        build += '\npopd\n';
      } else {
        build += '\n';
      }

      if (isPush) {
        push += '\necho "*Push ' + name + ' ' + imagename + '"\ndocker push ' + imagename + '\n';
        pull += '\necho "*Pull ' + name + ' ' + imagename + '"\ndocker pull ' + imagename + '\n';
      }
    }
  }
  function buildBrowserVersion(browser, version, data) {
    var versionExt = data['version-ext'] ? '-' + data['version-ext'] : '';
    var imageName = 'mulyoved/node-' + browser + '-' + version + ':' + getJsonData.image.version;
    var dockerFolder = browser === 'chrome' ? 'NodeChrome' : 'NodeFirefox';
    var notDockerFolder = browser !== 'chrome' ? 'NodeChrome' : 'NodeFirefox';
    var distFolder = 'dist/' + browser + '-' + version + '/';
    var browserCmd = browser === 'chrome' ? '/opt/google/chrome' + versionExt + '/chrome --product-version' : '/usr/bin/firefox --version';

    buildVersion += '\necho ' + getJsonData.image.version + ', ' + imageName + ', ' + browser + '-' + version +', `docker run ' + imageName + ' ' + browserCmd + '` >> browser-versions.txt';
    add(version, 'Node ' + browser, data.url ? 'Dockerfile.imageurl' : 'Dockerfile', distFolder + dockerFolder, imageName, false);
    add(version, 'Standalone '+ browser, distFolder + '/Standalone/Dockerfile', '', 'mulyoved/' + browser + '-' + version + ':' + getJsonData.image.version, true);
    add(version, 'Standalone Debug '+ browser, distFolder + '/StandaloneDebug/Dockerfile', '', 'mulyoved/' + browser + '-debug-' + version + ':' + getJsonData.image.version, true);

    taskList.push(gulp.src(['image-build/**/*', '!image-build/' + notDockerFolder + '/{,/**}', '!image-build/' + notDockerFolder + '/'])
      //.pipe(debug({title: 'input:'}))
      .pipe(replace('{{browser}}', browser))
      .pipe(replace('{{base.version}}', getJsonData.base.version))
      .pipe(replace('{{image.version}}', getJsonData.image.version))
      .pipe(replace('{{chrome.version}}', version))
      .pipe(replace('{{folder-name}}', browser + '-' + version))
      .pipe(replace('{{download.url}}', data.url))
      .pipe(replace('{{version-ext}}', versionExt))
      .pipe(replace('{{image-name}}', imageName))

      //.pipe(debug({title: 'output:'}))
      .pipe(gulp.dest('dist/' + browser + '-' + version)));
  }

  add('convert', 'Media Convert', 'images/MediaConvert/Dockerfile', null, 'mulyoved/media-convert:' + getJsonData.image.version, true);

  _.each(getJsonData.chrome, function(data, version) {
    if (!data.disabled) {
      buildBrowserVersion('chrome', version, data);
    }
  });

  _.each(getJsonData.firefox, function(data, version) {
    if (!data.disabled) {
      buildBrowserVersion('firefox', version, data);
    }
  });


  if (!fs.existsSync('./dist')){
    fs.mkdirSync('./dist');
  }

  fs.writeFileSync('./dist/build.sh', build);
  fs.writeFileSync('./dist/push.sh', push);
  fs.writeFileSync('./dist/pull.sh', pull);
  fs.writeFileSync('./dist/get-version.sh', buildVersion);

  return merge(taskList);
});

gulp.task('build-sh', ['build'], function () {
  return gulp.src(['dist/*.sh' ])
    .pipe(chmod(777))
    .pipe(gulp.dest('dist'));
});


