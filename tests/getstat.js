module.exports = new (function() {
  var testCases = this;

  testCases['call my phone'] = function (client) {
    var DEFAULT_TIMEOUT = 10000;
    var url = 'https://webrtc.github.io/samples/src/content/peerconnection/states/';

    client
      .url(url)
      .rtcStartTimer('myTimer')
      .injectGetStat('+++=== Log from Selenium (1)')
      .waitForElementVisible('body', 35000)
      .rtcScreenshot('page loaded')
      .waitForElementVisible('#startButton', DEFAULT_TIMEOUT)
      .rtcGetTimer('myTimer', function(timer) {
        client.rtcInfo('Time: %d', timer)
      })
      .rtcSetMetricFromTimer('timer-metric', 'myTimer')
      .rtcScreenshot('page loaded2')
      .injectFakeUserMedia('e:/testrtc/agent/sounds/343115988.mp4', 'video/webm', 10000)
      //.injectGetStat('+++=== Log from Selenium (2)')
      .click('#startButton')
      .waitForVideoHasLoaded('#video1', 10000)
      .click('#callButton')
      .pause(5000)
      .rtcScreenshot('call')
      .click('#hangupButton')
      .pause(1000) // let it close
      .retriveGetStat(function(answer) {
        client.info('answer', answer);
      })
  };
})();
