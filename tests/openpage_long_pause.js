module.exports = {
  "Long pause, to test kill" : function (client) {
    client
      .url('http://94.23.192.150:81/test_tsahi_webpage/')
      .waitForElementVisible('body', 1000)
      .assert.visible("#number_to_call")
      .assert.visible("#answer")
      .pause(100000)
      .end();
  }
};
