module.exports = new (function () {
  var testCases = this;

  testCases['call my phone'] = function (client) {

    console.log('env.ADMIN_URL', process.env.ADMIN_URL);
    console.log('env.ROOMID', process.env.RTC_SESSION_NAME);
    console.log('env.ROOM_MEMBER', process.env.RTC_IN_SESSION_ID);

    var roomMemberIdx = Number(process.env.RTC_IN_SESSION_ID);

    client
      .timeoutsAsyncScript(25000)
      .startRTCStatistic();

    if (roomMemberIdx === 0) {
      client
        .url('http://52.5.76.95:1234/')
        .waitForElementVisible('body', 2000)
        .waitForElementVisible('#webrtc_url', 2000)
        .getText('#webrtc_url', function (result) {
          this.assert.equal(typeof result, "object");
          this.assert.equal(result.status, 0);
          this.assert.ok(result.value.indexOf('http://52.5.76.95:1234/#') === 0);
          var roomUrl = result.value;
          console.log('Room URL', roomUrl);

          this
            .setRoomInfo('url', roomUrl, function (result) {
              console.log('Set room info', result);
            })
        });
    }
    else {
      client
        .waitForRoomInfo('url', function (roomUrl) {
          console.log('received roomUrl = ', roomUrl)
          client
            .url(roomUrl);
        }, 20000)
    }

    client
      .waitForAttribute('#loading_state', 'style', function (style) {
        console.log('#loading_state', style);
        return style && style.indexOf('display: none;') >= 0;
      }, 20000)
      .pause(90000);

    client
      .getRTCStatistic(function(result) {
        //console.log('webRTC Statistic ->', result);
      })
      .end();
  };
})();
