
module.exports = new (function() {
  var testCases = this;

  testCases['get random image'] = function (client) {
    client
      .timeoutsAsyncScript(30000)
      .url('http://lorempixel.com/640/480')
      .pause(2000)
      .execute(function() {
        console.log('Hello');
      },
      [1],
      function(result) {
        console.log('Test script completed');
      })
      .end();
  };
})();
