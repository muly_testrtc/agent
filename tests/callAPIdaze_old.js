module.exports = new (function() {
  var testCases = this;

  testCases['call my phone'] = function (client) {
    client
      .url('http://94.23.192.150:81/test_tsahi_webpage/')
      .waitForElementVisible('body', 1000)
      .setValue('#number_to_call', 972525503595)
      .waitForAttribute('#call', 'disabled', function (disabled) {
        console.log(disabled);
        return !disabled;
      }, 10000)
      .saveScreenshot('reports/s001.png')
      .click("#call")
      .waitForAttribute('#hangup', 'disabled', function (disabled) {
        console.log(disabled);
        return !disabled;
      }, 20000)

      //Play the music when call start
      .execute(function() {
        function load(url, callback) {
          var xhr;

          if(typeof XMLHttpRequest !== 'undefined') xhr = new XMLHttpRequest();
          else {
            var versions = ["MSXML2.XmlHttp.5.0",
              "MSXML2.XmlHttp.4.0",
              "MSXML2.XmlHttp.3.0",
              "MSXML2.XmlHttp.2.0",
              "Microsoft.XmlHttp"]

            for(var i = 0, len = versions.length; i < len; i++) {
              try {
                xhr = new ActiveXObject(versions[i]);
                break;
              }
              catch(e){}
            } // end for
          }

          xhr.onreadystatechange = ensureReadiness;

          function ensureReadiness() {
            if(xhr.readyState < 4) {
              return;
            }

            if(xhr.status !== 200) {
              return;
            }

            // all is well
            if(xhr.readyState === 4) {
              callback(xhr);
            }
          }

          xhr.open('GET', url, true);
          xhr.send('');
        }

        load('http://localhost:3000/play/sampleSoundFile/', function(xhr) {
          console.log(xhr);
        });

        return 2+2;
      },
      [2],
      function(result) {
        console.log('Executre result', result);
      })

      .waitForAttribute('#hangup', 'disabled', function (disabled) {
        console.log(disabled);
        return disabled;
      }, 120000)
      .end();
  };
})();
