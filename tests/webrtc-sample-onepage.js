var DEFAULT_TIMEOUT = 10000;
var url = 'https://webrtc.github.io/samples/src/content/peerconnection/states/';

client
  .rtcUrl(url)
  .rtcStartTimer('myTimer')
  .waitForElementVisible('body', 35000)
  .rtcScreenshot('page loaded')
  .waitForElementVisible('#startButton', DEFAULT_TIMEOUT)
  .rtcGetTimer('myTimer', function(timer) {
    client.rtcInfo('Time: %d', timer)
  })
  .rtcSetMetricFromTimer('timer-metric', 'myTimer')
  .rtcScreenshot('page loaded2')
  .click('#startButton')
  .waitForVideoHasLoaded('#video1', 10000)
  .click('#callButton')
  .pause(5000)
  .rtcScreenshot('call')
  .click('#hangupButton')
  .pause(1000) // let it close
