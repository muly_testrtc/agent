module.exports = new (function() {
  var testCases = this;

  testCases['call my phone'] = function (client) {
    client
      .timeoutsAsyncScript(30000)
      .url('http://94.23.192.150:81/test_tsahi_webpage/')
      .waitForElementVisible('body', 1000)
      .setValue('#number_to_call', '972525503595')
      .waitForAttribute('#call', 'disabled', function (disabled) {
        console.log(disabled);
        return !disabled;
      }, 10000)

      .mediaAgent('hello', null, function(result) {
        console.log('Hello', result);
      })

      .getRTCStatistic(function(result) {
        console.log('webRTC Statistic ->', result);
      })

      .end();
  };
})();
