module.exports = new (function() {
  var testCases = this;

  testCases['talkytest'] = function (client) {
    client.timeoutsAsyncScript(30000);
    client.startRTCStatistic();
    console.log('Talky demo start');
    client

    .url('https://talky.io/testrtcdemo')
    .waitForElementVisible('body', 1000)
    .pause(5000)
    .takeScreenshot('Talky 1')
    .takeScreenshot('Talky 2')
    .click("#join");
    console.log('After join');
    
    client
    .takeScreenshot('Talky 3')
    .pause(25000)

      .getRTCStatistic(function(result) {
        //console.log('webRTC Statistic ->', result);
      })

    .end();
  };
})();