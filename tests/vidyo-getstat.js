module.exports = new (function() {
  var testCases = this;

  testCases['call my phone'] = function (client) {
    var DEFAULT_TIMEOUT = 10000;
    var url = 'https://demo2.webrtc.vidyo.com/web/index.html?portalUri=https://main.demo2.vidyo.com&roomKey=fKzUm3R0CHMrt09XU8dlYtlyGI&driver=webrtc#/login';

    client
      .url(url)
      .injectGetStat('+++=== Log from Selenium (1)')
      .injectFakeUserMedia('e:/testrtc/agent/sounds/343115988.mp4', 'video/webm', 10000)
      .waitForElementVisible('body', 35000)
      .waitForElementVisible('#nicknameField', 60000)
      .setValue('#nicknameField', 'user2')
      .click('#loginJoinButton')
      .pause(20000)
      .retriveGetStat(function(answer) {
        client.info('answer', answer);
      })
      .end()
  };
})();
