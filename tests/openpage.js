module.exports = {
  "Just open APIDaze page, do nothing else" : function (client) {

    console.log("Start open page script");

    client
      .url('http://94.23.192.150:81/test_tsahi_webpage/')
      .waitForElementVisible('body', 1000)
      .assert.visible("#number_to_call")
      .assert.visible("#answer")
      .end();

    console.log("Completed open page script");
  }
};
