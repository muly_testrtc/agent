module.exports = {
  "Just open APIDaze page, do nothing else" : function (client) {
    client
      .url('http://94.23.192.150:81/test_tsahi_webpage/')
      .waitForElementVisible('body', 1000)
      .assert.visible("#number_to_call")
      .assert.visible("#answer")
      .waitForAttribute('#call', 'disabled', function (disabled) {
        //console.log(disabled);
        return !disabled;
      }, 5000)
      .end();
  }
};
