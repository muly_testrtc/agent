module.exports = new (function() {
  var testCases = this;

  testCases['call my phone'] = function (client) {
    client
      .timeoutsAsyncScript(30000)
      .startRTCStatistic()
      .url('https://beta.talky.io/testrtc')
      .waitForElementVisible('body', 10000)
      .waitForElementVisible('#join', 10000)
      .waitForAttribute('#join', 'disabled', function (disabled) {
        console.log(disabled);
        return !disabled;
      }, 10000)
      .pause(1000)
      .click("#join")
      .pause(20000)
      .click(".room-leaving")
      .getRTCStatistic(function(result) {
        console.log('Got webRTC Statistic');
      })
      .end();
  };
})();
