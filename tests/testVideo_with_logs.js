module.exports = new (function() {
  var testCases = this;

  testCases['call my phone'] = function (client) {
    client
      .timeoutsAsyncScript(30000)
      .startRTCStatistic()
      .url('https://beta.talky.io/testrtc')
      .log('Wait for body')
      .waitForElementVisible('body', 10000)
      .log('Wait for join')
      .waitForElementVisible('#join', 10000)
      .waitForAttribute('#join', 'disabled', function (disabled) {
        console.log(disabled);
        return !disabled;
      }, 10000)
      .log('join is enabled')

      .pause(1000)
      .click("#join")

      .log('join clicked')

      .pause(5000)

      .log('time for the call ended')

      .click(".room-leaving")

      .log('click room leaving')

      .getRTCStatistic(function(result) {
        console.log('Got webRTC Statistic', result);
      })

      .log('script end')

      .end();
  };
})();
