module.exports = new (function() {
  var testCases = this;

  testCases['call my phone'] = function (client) {
    client
      .timeoutsAsyncScript(30000)
      .url('http://94.23.192.150:81/test_tsahi_webpage/')
      .waitForElementVisible('body', 1000)
      .waitForAttribute('#call', 'disabled', function (disabled) {
        console.log(disabled);
        return !disabled;
      }, 10000)

      .mediaAgent('hello', null, function(result) {
        console.log('Hello', result);
      })


      .execute(function() {
        window.open('');
      }
      ,[0],
      function(result) {
        console.log('Execute result', result);
      })

      .window_handles(function(result) {
        console.log('handles',result);
        var handle = result.value[1];
        client.switchWindow(handle);
      })

      .url('chrome://webrtc-internals')


      /*
      .execute(function() {
        console.log('step1', this);
        console.log('step1.1', this.document);
        console.log('step1.2', this.document.getElementsByTagName);
        var body = this.document.getElementsByTagName('body')[0];
        console.log('step2', body);
        console.log('step2.1', body.innerHTML);
        var innerHTML = '<a href="chrome://webrtc-internals">RTC Stat</a>' + body.innerHTML;
        console.log('step3', innerHTML);
        body.innerHTML = innerHTML;
        console.log('step4');
      }
      ,[0],
      function(result) {
        console.log('Execute result', result);
      })
      */
      //.end();
  };
})();
