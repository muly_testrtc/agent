
module.exports = new (function() {
  var testCases = this;

  testCases['get random image'] = function (client) {
    console.log('env.TEST_RESULTS_PATH', process.env.TEST_RESULTS_PATH);
    client
      .timeoutsAsyncScript(35000)
      .url('http://85fc8ccd760a621e9d81-e9638d787a6e32e99181ab75ac8c3328.r68.cf2.rackcdn.com/test_page.html')
      .waitForElementVisible('body', 35000)
      .waitForElementVisible('#download-sample', 35000)
      .takeScreenshot('screen1')
      .click("#download-sample")
      .pause(500)
      .mediaAgent('get-webrtc-stat', {INFO: "no-logs"}, function(result) {
        console.log('webRTC Stat->', result);
      })
      .pause(500)
      .end();
  };
})();
