module.exports = new (function() {
  var testCases = this;

  testCases['call my phone'] = function (client) {
    client
      .timeoutsAsyncScript(30000)
      .startRTCStatistic()
      .url('http://94.23.192.150:81/test_tsahi_webpage/')
      .waitForElementVisible('body', 1000)
      .setValue('#number_to_call', '972525503xxx')
      .waitForAttribute('#call', 'disabled', function (disabled) {
        return !disabled;
      }, 10000)


      .click("#call")

      //Wait for call to connect
      .waitForAttribute('#hangup', 'disabled', function (disabled) {
        return !disabled;
      }, 20000)

      //Wait for call to disconnect
      .waitForAttribute('#hangup', 'disabled', function (disabled) {
        return disabled;
      }, 120000)

      .getRTCStatistic(function(result) {
      })

      .end();
  };
})();
