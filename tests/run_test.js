var fs = require('fs');

module.exports = new (function() {
  var testCases = this;

  testCases['get random image'] = function (client) {
    console.log('env.TEST_RESULTS_PATH', process.env.TEST_RESULTS_PATH);
    client
      .timeoutsAsyncScript(35000);
    try {
      client
        .info('Start test')
        .url('http://85fc8ccd760a621e9d81-e9638d787a6e32e99181ab75ac8c3328.r68.cf2.rackcdn.com/test_page.html')
        .info('Open URL')
        .waitForElementVisible('body', 35000)
        .waitForElementVisible('#download-sample', 35000)
        .takeScreenshot('screen1')
        .info('Click')
        .click("#download-sample")
        .pause(3000) // need to wait to file to finish download
        .info('After Wait')
        .getLog('browser', function(logEntriesArray) {
          var fileName = process.env.TEST_RESULTS_PATH + '/browser_logs.json';
          fs.writeFileSync(fileName, JSON.stringify(logEntriesArray));
          console.log('Write logs to', fileName, logEntriesArray);
        })
    } catch(err) {
      console.warn("TEST_RTC_ERROR:", err);
      //'    client.info("TEST_SCRIPT_EXCEPTION", err);' +
      throw err;
    } finally {
      client.end();
    }
  };
})();
