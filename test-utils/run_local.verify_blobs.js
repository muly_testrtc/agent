/**
 * The purpose of this script is to check uploaded blobs after running run_local script
 * Works with both SFTP and Azure
 */

'use strict';

var fs = require('fs'),
    AzureBlobStorage = require('azureblob-upload-node'),
    SSH2Client = require('ssh2').Client;

var optionsFileName = './test-utils/chrome-run_local.stdin.json';
// var optionsFileName = './test-utils/firefox-run_local.stdin.json';
var testFileName = './test-utils/run_local.test.js';

var testOptions = JSON.parse(fs.readFileSync(optionsFileName, 'utf8')),
    blobOptions = testOptions.blobOptions,
    sftpOptions = testOptions.sftpOptions;

checkAzure();
if (testOptions.options.useSFTP) {
    checkSFTP();
}

function checkAzure() {
    console.log('Azure. blobOptions:', blobOptions);

    var storage = new AzureBlobStorage(process.env.AZURE_STORAGE_CONNECTION_STRING, blobOptions.container);
    storage.list(blobOptions.folder + ':').then(function(list) {
      console.log('Azure total items: %d\n', list.length);
      list.map(function(item) {
        console.log('%s (%s) - %d bytes.\nUploaded on %s\nURL: %s\n', item.fullBlobName, item.properties['content-type'], item.properties['content-length'], item.properties['last-modified'], storage.getURL(item.fullBlobName));
      });
    }).catch(function(err) {
      console.log('Error:', err);
    });
}


function checkSFTP() {
    console.log('SFTP. sftpOptions:', sftpOptions);

    var conn = new SSH2Client();
    conn.on('ready', () => {
        conn.sftp((err, sftp) => {
            if (err) {
                throw err;
            }

            sftp.stat(sftpOptions.path, (err, stats) => {
                if (err) {
                    throw new Error('Remote SFTP path doesn\'t exist');
                }
                if (!stats.isDirectory()) {
                    throw new Error('Remote SFTP path doesn\'t point to directory');
                }

                sftp.readdir(sftpOptions.path, (err, list) => {
                    if (err) {
                        throw new Error('Can\'t readdir remote SFTP path');
                    }

                    list = list.filter((item) => !item.attrs.isDirectory());
                    
                    console.log('SFTP total items: %d\n', list.length);

                    list.map((item) => {
                        console.log('%s - %d bytes.\nUploaded on %s\n', item.filename, item.attrs.size, new Date(item.attrs.mtime * 1000));
                    });

                    conn.end();
                });
            });
        });
    }).on('error', (err) => {
        console.log('+SSH', 'error', err);
    });

    conn.connect(sftpOptions);
}
