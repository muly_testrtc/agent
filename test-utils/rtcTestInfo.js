if (!('_testInfo' in global )) {
  global._testInfo = {};
  global._testOptions = {};
  global._timers = {};
  global._sync = {};
  global._screenshots = [];
  global._messages = [];
}


module.exports.setInfo = function(varName, varValue) {
  global._testInfo[varName] = varValue;
};

module.exports.addValue = function(category, varName, val) {
  if (!global._testInfo[category]) {
    global._testInfo[category] = {};
  }

  var cat = global._testInfo[category];
  if (!cat[varName]) {
    cat[varName] = [];
  }

  cat[varName].push(val);
};

module.exports.get = function() {
  return global._testInfo;
};

//Muly to clean this
module.exports.getTimers = function() {
  return global._timers;
};

module.exports.getTestOptions = function() {
  return global._testOptions
};

module.exports.setTestOptions = function(value) {
  global._testOptions = value;
};

module.exports.setSessionValue = function(varName, varValue) {
  global._sync[varName] = varValue;
  console.log('setSessionValue', varName, varValue, global._sync);
};

module.exports.getSessionValue = function(varName) {
  console.log('getSessionValue', varName, global._sync);
  return global._sync[varName];
};

module.exports.addScreenshot = function(filename, url) {
  global._screenshots.push({
    filename: filename,
    url: url
  });
};

module.exports.getScreenshots = function() {
  return global._screenshots;
};

module.exports.addMessage = function(topic, msgType, msg) {
  global._messages.push({
    topic: topic,
    msgType: msgType,
    message: msg
  });
};

module.exports.getMessages = function() {
  return global._messages;
};
