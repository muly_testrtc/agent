"use strict";

const os = require('os');
const fs = require('fs');
const spawn = require('child_process').spawn;

const DEFAULT_TIMEOUT = 10000;

class RTCPerformanceUtil {

  constructor(timeout) {
    this._timeout = +timeout || DEFAULT_TIMEOUT;
    this._samples = [];
  }

  start() {
    // Fire timer function to collect the first sample
    this.onTimer();
    this._timeoutID = setInterval(this.onTimer.bind(this), this._timeout);
  }

  onTimer() {
    this.readCpuUsageAsync((err, cpuUsage) => {
      if (err) {
        // Ignore error and set cpuUsage to null
        cpuUsage = null;
      }

      this.readMemoryUsageAsync((err, memoryUsage) => {
        if (err) {
          // Ignore error
          memoryUsage = null;
        }

        this._samples.push({
          ts: +Date.now(),
          cpu: cpuUsage,
          memory: memoryUsage
        });
      });
    });
  }

  stop(filename) {
    clearInterval(this._timeoutID);
    fs.writeFileSync(filename, new Buffer(JSON.stringify(this._samples), 'utf8'));
    console.log(JSON.stringify(this._samples));
  }

  readMemoryUsageAsync(cb) {
    let prc = spawn('free', []);

    prc.stdout.setEncoding('utf8');
    prc.stdout.on('data', (data) => {
      let lines = data.toString().split(/\n/g),
          line = lines[1].split(/\s+/),
          total = parseInt(line[1]),
          free = parseInt(line[3]),
          buffers = parseInt(line[5]),
          cached = parseInt(line[6]),
          actualFree = free + buffers + cached,
          percentUsed = parseFloat(((1 - (actualFree / total)) * 100).toFixed(2));

      cb(null, percentUsed);
    });

    prc.on('error', cb);
  }

  /**
   * Function is ported from https://github.com/husanu/nodejs-cpu-usage/blob/master/cpuusage.js
   */
  readCpuUsageAsync(cb) {
    if (!this._cpuUsage) {
      this._cpuUsage = {
        prevTotal: 0,
        prevIdle: 0,
        prevLoad: 0
      };
    }

    fs.readFile('/proc/stat', (err, data) => {
      if (err) {
        // Nevermind
        return cb(err);
      }

      let dRaw = data.toString().split(' '),
          d = [],
          idx = 1,
          count = 0;

      while (count < 4) {
        let t = parseInt(dRaw[idx]);
        if (t) {
          count++;
          d.push(t);
        }
        idx++;
      }
      
      let idle = parseInt(d[3]),
          total = parseInt(d[0]) + parseInt(d[1]) + parseInt(d[2]),
          load = 0;
      
      if (this._cpuUsage.prevTotal) {
        load = Math.round( (total - this._cpuUsage.prevTotal) / (total + idle - this._cpuUsage.prevTotal - this._cpuUsage.prevIdle) * 100 );
        cb(null, load);
      } else {
        // The first run - call this function once again
        this.readCpuUsageAsync(cb);
      }
      
      this._cpuUsage = {
        prevTotal: total,
        prevIdle: idle,
        prevLoad: load
      };
    });
  }

}

module.exports = RTCPerformanceUtil;
