(function () {
  'use strict';

  var debug = require('debug')('run_test');
  debug = console.info.bind(console);
  debug('run_local version 0.7.0');


  // var optionsFileName = './test-utils/chrome-run_local.stdin.json';
  var optionsFileName = './test-utils/firefox-run_local.stdin.json';
  var testFileName = './test-utils/run_local.test.js';
  //var testFileName = './tests/vidyo.js';
  // var testFileName = './tests/webrtc-sample-onepage.js';
  var scriptTemplateFileName = './test-utils/run_local.script.js';

  var _ = require('lodash');
  var fs = require('fs');
  var path = require('path');
  var testExecutions = require('./test-executions');

  var agentPath = path.resolve(__dirname, '..');
  var firefoxExtensionPath = path.join(agentPath, './extensions/firefox/@testrtcapi-1.0.xpi').replace(/\\/g,'\\\\');


  // Copy from server
  var preprocessTestScript = function(script, browserEnv) {
    // Backward compatible

    if (script.indexOf('module.exports') < 0) {
      var templateData = {
        firefox: (browserEnv === 'firefox'),
        script: script
      };

      // Compose script from the template
      script = _.template(fs.readFileSync(scriptTemplateFileName, 'utf8'))(templateData);
    }

    return script;
  };

  var testOptions = JSON.parse(fs.readFileSync(optionsFileName, 'utf8'));
  var webRTC_dir = testOptions.browserDownloadPath || agentPath; // ??

  testOptions.testScript = preprocessTestScript(fs.readFileSync(testFileName, 'utf8'), testOptions.browserEnv);

  testExecutions.init({
    webRTC_dir: webRTC_dir,
    basePath: agentPath
  });

  testExecutions.prepareTestEnv();
  testExecutions.parseStdin(JSON.stringify(testOptions, null, 4));
}());
