<% if (firefox) { %>
var FirefoxProfile = require("firefox-profile");
function setProfile(browser, profile, callback) {
  profile.encoded(function (encodedProfile) {
    browser.options.desiredCapabilities["firefox_profile"] = encodedProfile;
    callback();
  });
}
<% } %>

module.exports = new (function() {
  var testCases = this;

  <% if (firefox) { %>
  testCases.before = function(browser, done) {
    var profile = new FirefoxProfile();
    profile.addExtension(browser.globals.extensionPath, function() {
      profile.setPreference('browser.startup.homepage', 'about:blank');
      profile.setPreference('browser.startup.homepage_override.mstone', 'ignore');
      profile.setPreference('browser.newtab.url', 'about:blank');
      profile.setPreference('security.mixed_content.block_active_content', false);
      setProfile(browser, profile, done);
    });
  };
  <% } %>

  testCases.after = function(client, done) {
    var failed = (client.currentTest.results.errors || client.currentTest.results.failed);

    client
      .testRTCEnd(failed)
      .end(done);
  };

  testCases.test = function (client) {
  client.timeoutsAsyncScript(35000).testRTCStart();
    try {
      <%= script %>
    } catch(err) {
      console.info("TEST_RTC_ERROR:", err);
      throw err;
    }
  };
})();
