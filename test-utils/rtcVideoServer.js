"use strict";

const http = require('http');
const fs = require('fs');
const path = require('path');


class RTCVideoServer {

  constructor(mediaPath) {
    this._mediaPath = mediaPath;
    this._server = http.createServer(this.onRequest.bind(this));

    let mediaExt = path.extname(mediaPath).substring(1);
    if (mediaExt === 'mp4' || mediaExt === 'webm') {
      this._mediaMimeType = `video/${mediaExt}`;
    } else if (mediaExt === 'wav' || mediaExt === 'mp3') {
      this._mediaMimeType = `audio/${mediaExt}`;
    }

    fs.stat(mediaPath, (err, stats) => {
      if (err) {
        throw err;
      }

      this._mediaSize = stats.size;
    });
  }

  start(port) {
    this._server.listen(port);
  }

  onRequest(req, res) {
    if (req.url !== '/stream') {
      res.writeHead(404, { "Content-Type": "text/plain" });
      return res.end('Not found');
    }

    let headers = {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Headers": "X-Requested-With"
    };

    if (this._mediaMimeType) {
      headers["Content-Type"] = this._mediaMimeType;
    }

    let range = req.headers.range;
    if (!range) {
      range = `bytes=0-`;
    }

    let positions = range.replace(/bytes=/, "").split("-"),
        start = +positions[0],
        total = this._mediaSize,
        end = positions[1] ? +positions[1] : start + total - 1;

    let chunksize = (end - start) + 1;

    headers["Content-Range"] = "bytes " + start + "-" + end + "/" + total;
    headers["Accept-Ranges"] = "bytes";

    let movieStream = fs.createReadStream(this._mediaPath, { start: start, end: end });
    movieStream.on('open', function () {
      res.writeHead(206, headers);
      movieStream.pipe(res);
    });

    movieStream.on('error', function (err) {
      res.end(err);
    });
  }

  stop() {
    this._server.close();
  }

}

module.exports = RTCVideoServer;
