"use strict";

const async = require('async');
const path = require('path');
const os = require('os');
const fs = require('fs');
const archiver = require('archiver');

const AzureBlobStorage = require('azureblob-upload-node');
const SSH2Client = require('ssh2').Client;

class Uploader {

  constructor() {
    this._queue = [];
  }

  /**
   * Push a file specified by absolute path to the internal queue
   */
  queue(name, path, options) {
    this._queue.push({
      name: name,
      path: path,
      options: options
    });
  }

  /**
   * Do upload
   */
  uploadAsync(cb) {}

}

class AzureUploader extends Uploader {

  constructor(connectionString, container) {
    super();
    this._client = new AzureBlobStorage(connectionString, container);
    this._client.setRetriesCount(3);
  }

  uploadAsync(cb) {
    // Upload each file step by step and then call the cb

    async.mapSeries(this._queue, (item, done) => {
      let remoteName = item.options.azure.folder + ':' + item.name;
      let options = {
        getURL: !!item.options.azure.getURL,
        compress: !!item.options.azure.compress
      };

      if (item.options.azure.metadata) {
        options.metadata = item.options.azure.metadata;
      }

      if (item.options.azure.contentType) {
        options.contentType = item.options.azure.contentType;
      }

      this._client
        .save(remoteName, item.path, options)
        .then((answer) => {
          done(null, { name: item.name, remoteName: remoteName, url: answer });
        }).catch((err) => {
          done(err);
        });
    }, (err, results) => {
      // Filter out empty results
      results = results.filter((item) => !!item);
      cb(err, results);
    });
  }

}

class SFTPUploader extends Uploader {

  constructor(options, extraOptions) {
    super();
    if (!options.path) {
      throw new Error('Remote path for SFTP connection is not specified');
    }

    this._options = options;
    this._extraOptions = extraOptions;
    this._client = new SSH2Client();
  }

  uploadAsync(cb) {
    let resultingPath = this._options.path;
    if (this._extraOptions && this._extraOptions.folder) {
      resultingPath = path.join(resultingPath, this._extraOptions.folder);
    }

    this._client.on('ready', () => {
      this._client.sftp((err, sftp) => {
        if (err) {
          this._client.end();
          return cb(err, []);
        }

        sftp.stat(resultingPath, (err, stats) => {
          if (err) {
            if (err.code !== 2) {
              // Unknown error
              this._client.end();
              return cb(err);
            } else if (err.code === 2 && !this._extraOptions.folder) {
              // ENOENT
              this._client.end();
              return cb(new Error('Remote SFTP path doesn\'t exist'));
            }

            return this._recursiveMkdir(sftp, this._options.path, this._extraOptions.folder, (err) => {
              if (err) {
                this._client.end();
                return cb(err);
              }

              this._doUpload(sftp, resultingPath, cb);
            });
          }

          if (!stats.isDirectory()) {
            this._client.end();
            return cb(new Error('Remote SFTP path doesn\'t point to directory'));
          }

          this._doUpload(sftp, resultingPath, cb);
        });
      });
    });

    this._client.on('error', cb);

    try {
      this._client.connect(this._options);
    } catch (e) {
      cb(e);
    }
  }

  _doUpload(sftp, resultingPath, cb) {
    if (this._extraOptions.zip) {
      return this._doUploadZip(sftp, this._extraOptions.zip, resultingPath, cb);
    }

    async.mapSeries(this._queue, (item, done) => {
      let remoteName = path.join(resultingPath, item.name);
      sftp.fastPut(item.path, remoteName, {}, (err) => {
        if (err) {
          return done(err);
        }

        done(null, { name: item.name, remoteName: remoteName });
      });
    }, (err, results) => {
      this._client.end();
      cb(err, results);
    });
  }

  _doUploadZip(sftp, zipFilename, resultingPath, cb) {
    let zipPath = path.join(os.tmpdir(), zipFilename),
        zipWriteStream = fs.createWriteStream(zipPath),
        zip = archiver('zip');

    zip.on('error', (err) => {
      cb(err);
    });

    zipWriteStream.on('close', () => {
      // ZIP has been written
      let remoteName = path.join(resultingPath, zipFilename);
      sftp.fastPut(zipPath, remoteName, {}, (err) => {
        this._client.end();
        if (err) {
          return cb(err);
        }

        return cb(null, [{ remoteName: remoteName }]);
      });
    });

    zip.pipe(zipWriteStream);

    this._queue.forEach((item) => {
      zip.append(fs.createReadStream(item.path), {
        name: item.name
      });
    });
    
    zip.finalize();
  }

  _recursiveMkdir(sftp, root, folder, cb) {
    let toMake = folder.split(path.sep).filter((el) => !!el);
    if (!toMake.length) {
      return cb(null);
    }

    let next = path.join(root, toMake.shift());
    sftp.mkdir(next, (err) => {
      if (err && err.code !== 4) {
        return cb(err);
      }

      this._recursiveMkdir(sftp, next, toMake.join(path.sep), cb);
    });
  }

}

module.exports = {
  AzureUploader: AzureUploader,
  SFTPUploader: SFTPUploader
};