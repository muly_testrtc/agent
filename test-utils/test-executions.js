var debug = require('debug')('test-exec');
debug = console.info.bind(console);

//var heapdump = require('heapdump');
var profiler;

var Q = require('q');
var async = require('async');
var fs = require('fs');
var util = require('util');
var path = require('path');
var mkdirp = require('mkdirp');
var os = require('os');
var exec = require('child_process').exec;
var spawn = require('child_process').spawn;
var cpuStats = require('cpu-stats');
var libCpuUsage = require('cpu-usage');
var rtcTestInfo = require('./rtcTestInfo');

var results = { status: 'started' };
var jsonTestOptions = {};
var resultPath;
var enableWriteSnapshot = false;

var basePath = '/agent';
var tmpFolder = './tmp';
var scriptPath = './tmp/test.js';
var confPath = './tmp/nightwatch.json';
var webRTC_dir;

var runner;
var Nightwatch = require('nightwatch/lib');
var RTCUploader = require('./rtcUploader');
var RTCVideoServer = require('./rtcVideoServer');
var RTCPerformanceUtil = require('./rtcPerformanceUtil');

var webRTC_fileName = 'webrtc_internals_dump.txt',
    getstatLog_fileName = 'getstat_logs.json',
    browserLog_fileName = 'browser_logs.json',
    reverseWav_fileName = 'reverse.wav',
    performance_fileName = 'performance.json',
    pvqaReport_fileName = 'pvqa.json';

module.exports.init = function(options) {
  webRTC_dir = options.webRTC_dir;
  basePath = options.basePath || basePath;
};

var ALLOWED_COMMANDS = ['df', './bin/comcast', 'comcast', 'iptables', 'ip6tables', 'ls', 'tc'];

var IPTABLES_RESET_COMMANDS = [
  { name: 'iptables_reset_1', cmd: 'iptables -P INPUT ACCEPT' },
  { name: 'iptables_reset_2', cmd: 'iptables -P FORWARD ACCEPT' },
  { name: 'iptables_reset_3', cmd: 'iptables -P OUTPUT ACCEPT' },
  { name: 'iptables_reset_4', cmd: 'iptables -t nat -F' },
  { name: 'iptables_reset_5', cmd: 'iptables -t mangle -F' },
  { name: 'iptables_reset_6', cmd: 'iptables -F' },
  { name: 'iptables_reset_7', cmd: 'iptables -X' }
];

var COMCAST_RESET_COMMANDS = [
  { name: 'comcast_reset_1', cmd: './bin/comcast  --mode stop' }
];

function progress(msg) {
  console.info(util.format('TEST-RTC-MESSAGE: %s', JSON.stringify({
    msgType: 'progress',
    text: msg
  })));
}

function collectMessages() {
  if (!results.additonalMessages) {
    results.additonalMessages = [];
  }

  var messages = rtcTestInfo.getMessages();
  results.additonalMessages = results.additonalMessages.concat(messages);
}

function addMessage(topic, msgType, msg) {
  if (!results.additonalMessages) {
    results.additonalMessages = [];
  }

  results.additonalMessages.push({
    topic: topic,
    msgType: msgType,
    message: msg
  });

  debug('addMessage', topic, msgType, msg);
}


function writeSnapshot(id, cb) {
  if (enableWriteSnapshot) {
    var memDiffFiles = path.join(resultPath, 'memdiff.' + id + '.json');

    /*
     heapdump.writeSnapshot(memDiffFiles);
     if (fs.existsSync(memDiffFiles)) {
     results.memDiffLogFile = results.memDiffLogFile || [];
     } else {
     debug('writeSnapshot failed', memDiffFiles);
     }
     */

    var snapshot = profiler.takeSnapshot();
    snapshot.export(function(error, result) {
      if (error) {
        debug('Failed to export profiler.takeSnapshot', id);
      } else {
        fs.writeFileSync(memDiffFiles, result);
        results.memDiffLogFile = results.memDiffLogFile || [];
        results.memDiffLogFile.push(memDiffFiles);
      }
      snapshot.delete();
      if (cb) cb(error);
    });
  } else {
    if (cb) cb();
  }
}

function copyFile(source, target, cb) {
  var cbCalled = false;
  debug('copyFile1', source, target);

  var rd = fs.createReadStream(source);
  rd.on("error", function(err) {
    done(err);
  });
  var wr = fs.createWriteStream(target);
  wr.on("error", function(err) {
    done(err);
  });
  wr.on("close", function(ex) {
    done();
  });
  rd.pipe(wr);
  debug('copyFile2', source, target);

  function done(err) {
    debug('copyFile', source, target, err.stack);
    if (!cbCalled) {
      cb(err);
      cbCalled = true;
    }
  }
}

function writeCPUProfile(id, cb) {
  if (enableWriteSnapshot) {
    var memDiffFiles = path.join(resultPath, 'memdiff.' + id + '.json');
    var profile1 = profiler.stopProfiling();
    profile1.export(function(error, result) {
      if (error) {
        debug('Failed to export profiler.takeSnapshot', id);
      } else {
        fs.writeFileSync(memDiffFiles, result);
        results.memDiffLogFile = results.memDiffLogFile || [];
        results.memDiffLogFile.push(memDiffFiles);
      }
      profile1.delete();
      if (cb) cb(error);
    });
  } else {
    if (cb) cb();
  }
}

function getExpectedDownloadDirectory(cb) {
  var directory;

  if (process.platform === 'linux') {
    cb(null, path.resolve(webRTC_dir, '..'));


    /*
    // For Linux, it's the best way to detect download directory
    var output = spawn('xdg-user-dir', ['DOWNLOAD']);
    output.stdout.on('data', function(data) {
      directory = data;
    });
    output.on('close', function() {
      cb(null, directory);
    });
    return;
    */
  }

  if (process.platform === 'darwin') {
    // For OSX, simply join home directory and "Downloads"
    return cb(null, path.join(process.env.HOME, 'Downloads'));
  }

  if (process.platform === 'win32') {
    return cb(new Error('Windows not yet supported'));
  }
}

module.exports.reportMemoryStatus = function() {
  var spawn = require('child_process').spawn;
  var prc = spawn('free', []);
  var os = require('os');

  prc.stdout.setEncoding('utf8');
  prc.stdout.on('data', function (data) {
    var lines = data.toString().split(/\n/g),
      line = lines[1].split(/\s+/),
      total = parseInt(line[1], 10),
      free = parseInt(line[3], 10),
      buffers = parseInt(line[5], 10),
      cached = parseInt(line[6], 10),
      actualFree = free + buffers + cached,
      memory = {
        total: total,
        used: parseInt(line[2], 10),
        free: free,
        shared: parseInt(line[4], 10),
        buffers: buffers,
        cached: cached,
        actualFree: actualFree,
        percentUsed: parseFloat(((1 - (actualFree / total)) * 100).toFixed(2)),
        comparePercentUsed: ((1 - (os.freemem() / os.totalmem())) * 100).toFixed(2)
      };
    debug('memory', memory.percentUsed);

    if (memory.percentUsed > 90) {
      handleError('out-of-memory', 'Not enough memory on agent machine: ' + memory.percentUsed + '%');
    }
  });

  prc.on('error', function (err) {
    debug('[ERROR] Free memory process', err);
  });
};

module.exports.parseStdin = function(data) {
  function executeCommand(cmd) {
    if (cmd.command === 'SIGKILL') {
      handleError('command', cmd.message || 'Received SIGKILL');
    }
    else if (cmd.command === 'set-session-value') {
      debug(util.format('rtcTestInfo.setSessionValue %s %s', cmd.varName, cmd.varValue));
      rtcTestInfo.setSessionValue(cmd.varName, cmd.varValue);
    }
  }

  try {
    debug('parseStdin', data);

    var json = JSON.parse(data);
    if (json.command) {
      executeCommand(json);
    } else {
      jsonTestOptions = json;
      resultPath = jsonTestOptions.resultPath;
      enableWriteSnapshot = jsonTestOptions.options.enableWriteSnapshot;
      if (enableWriteSnapshot) {
        //need to add the package.json, not build good from jenkins so removed fro now "v8-profiler": "^5.3.2"
        profiler = require('v8-profiler');
        profiler.startProfiling('start', true);
      }
      debug('Received Test Option', jsonTestOptions);


      var browserEnv = jsonTestOptions.browserEnv;
      var audioPath = jsonTestOptions.audioPath;
      var videoPath = jsonTestOptions.videoPath;
      var chromeExtensionPath = path.join(basePath, 'extensions', 'chrome');

      if (audioPath && !fs.existsSync(audioPath)) {
        handleError('settings', 'Audio file not found: ' + audioPath);
      }

      if (videoPath && !/^https?:/.test(videoPath) && !fs.existsSync(videoPath)) {
        handleError('settings', 'Video file not found ' + videoPath);
      }

      if (!resultPath) {
        handleError('settings', 'Missing value in resultPath');
      } else {
        mkdirp(resultPath, function (err) {
          if (err) {
            handleError(err, 'Failed to create iteration result path' + resultPath);
          } else {
            mkdirp(tmpFolder, function (err) {
              if (err) {
                handleError(err, 'Failed to create tmp path:' + tmpFolder);
              } else {
                writeSnapshot('start');
                process.env["TEST_RESULTS_PATH"] = jsonTestOptions.resultPath;
                //resultPath = resultPath + '/itr_' + jsonTestOptions.iteartionOptions.runIndex;
                //env.TEST_RESULTS_PATH = resultPath;
                try {
                  fs.writeFileSync(scriptPath, jsonTestOptions.testScript);
                } catch (e) {
                  handleError(e, 'Failed to create test script file' + scriptPath);
                }

                try {
                  var confTemplate = require('./nightwatch.template.json');

                  if (browserEnv === 'chrome') {
                    var chromeArgs = confTemplate["test_settings"]["chrome"]["desiredCapabilities"]["chromeOptions"]["args"];
                    if (jsonTestOptions.options['chrome-cli']) {
                      var parts = jsonTestOptions.options['chrome-cli'].split(',');
                      parts.forEach(function(part) {
                        chromeArgs.push(part);
                      })
                    }

                    if (jsonTestOptions.browserSetup) {
                      if (jsonTestOptions.browserSetup.args) {
                        jsonTestOptions.browserSetup.args.forEach(function (part) {
                          chromeArgs.push(part);
                        })
                      }

                      if (jsonTestOptions.browserSetup.binary) {
                        confTemplate["test_settings"]["chrome"]["desiredCapabilities"]["chromeOptions"]["binary"] = jsonTestOptions.browserSetup.binary;
                      }
                    }

                    if (jsonTestOptions.audioPath && !jsonTestOptions.options.disableAudio) {
                      chromeArgs.push('use-file-for-fake-audio-capture=' + jsonTestOptions.audioPath);
                      /*
                       checksum.file(jsonTestOptions.audioPath, function (err, sum) {
                       if (err) console.log('Failed to calculate checksum of audio file', jsonTestOptions.audioPath, err);
                       debug('audio file checksum', jsonTestOptions.audioPath, sum);
                       })
                       */
                    }
                    if (jsonTestOptions.videoPath && !jsonTestOptions.options.disableVideo) {
                      chromeArgs.push('use-file-for-fake-video-capture=' + jsonTestOptions.videoPath);
                      /*
                       checksum.file(jsonTestOptions.videoPath, function (err, sum) {
                       if (err) console.log('Failed to calculate checksum of video file', jsonTestOptions.videoPath, err);
                       debug('video file checksum', jsonTestOptions.videoPath, sum);
                       })
                       */
                    }

                    if (jsonTestOptions.useGetStat) {
                      //debug('__filename', __filename);
                      chromeArgs.push('load-extension=' + chromeExtensionPath);
                    }

                    debug('chromeOptions', confTemplate["test_settings"]["chrome"]["desiredCapabilities"]["chromeOptions"]);

                    // Audio saving options

                    if (jsonTestOptions.options.mediaStorage || jsonTestOptions.options['voice-quality']) {
                      process.env["AUDIO_CAPTURE"] = 1;
                      debug('Set AUDIO_CAPTURE');
                    }

                  } else if (browserEnv === 'firefox') {
                    var fireFoxArgs = confTemplate["test_settings"]["firefox"];
                    var globals = fireFoxArgs["globals"];
                    var mediaPath = null;
                    if (jsonTestOptions.videoPath && !jsonTestOptions.options.disableVideo) {
                      mediaPath = jsonTestOptions.videoPath;
                    } else if (jsonTestOptions.audioPath && !jsonTestOptions.options.disableAudio) {
                      mediaPath = jsonTestOptions.audioPath;
                    }

                    if (mediaPath) {
                      var mediaExt = path.extname(mediaPath).substring(1);
                      if (mediaExt !== 'mp4' &&
                        mediaExt !== 'webm' &&
                        mediaExt !== 'mp3' &&
                        mediaExt !== 'wav') {
                        handleError('settings', 'Unknown media file format: ' + mediaPath);
                      }

                      globals["fakeVideoPath"] = mediaPath;
                      globals["fakeVideoMimeType"] = 'video/' + mediaExt;
                    } else {
                      globals["fakeVideoPath"] = '';
                      globals["fakeVideoMimeType"] = 'none';
                    }

                    var firefoxExtensionPath = path.join(basePath, 'extensions/firefox/@testrtcapi-1.0.0.xpi');
                    globals["extensionPath"] = firefoxExtensionPath;
                  }

                  // Test files saving options

                  if (!jsonTestOptions.blobOptions) {
                    throw new Error('Blob options are not defined (both blobOptions.container and blobOptions.folder should be set)');
                  }
                  if (jsonTestOptions.options.useSFTP && !jsonTestOptions.sftpOptions) {
                    throw new Error('SFTP options are not defined');
                  }

                  //debug('confTemplate', JSON.stringify(confTemplate, null, 4));
                  fs.writeFileSync(confPath, JSON.stringify(confTemplate, null, 4));

                  executeCmd(jsonTestOptions.testScript, jsonTestOptions.commandExec, jsonTestOptions.allowedCommands).then(function() {
                    // Wait for commands to complete

                    rtcTestInfo.setTestOptions(jsonTestOptions);

                    writeSnapshot('waitForSelenium');
                    waitForSelenium();
                  }).catch(function(err) {
                    handleError(err, 'Error occured while running commands');
                  });
                } catch (err) {
                  handleError(err, 'Failed to create nightwatch configuration file:' + confPath);
                }
              }
            });
          }
        });
      }
    }
  } catch (e) {
    handleError(e.stack, 'Failed to parse test options: ' + data);
  }
};

module.exports.prepareTestEnv = function() {
  setInterval(function() {
    debug('Internal interval check, I am alive!', new Date());
  }, 10000);

  var memoryPctl = Math.round(100 * os.freemem() / os.totalmem());
  debug('memory %', memoryPctl , Math.round(os.freemem()/1024/1024), Math.round(os.totalmem()/1024/1024), 'load', os.loadavg());
  /*
   if (memoryPctl < 10) {
   handleError('out-of-memory', 'Not enough memory on agent machine: ' + memoryPctl + '%');
   }
   */
  //debug('env', process.env);

  /*
   if (!fs.existsSync(localJsValue)) {
   basePath = path.resolve('agent/');
   localJsValue = path.join(basePath, SETTINGS_JS_FILE);
   if (!fs.existsSync(localJsValue)) {
   handleError('settings', 'Failed to find nightwatch settings file:' + localJsValue);
   }
   }
   */

  debug('change cwd', basePath);
  process.chdir(basePath);
}

function writeResult() {
  process.stderr.write(JSON.stringify(results)+'\n');
  debug('writeResult', results);
}

function handleError(err, msg) {
  debug('SYSTEM_RTC_ERROR: [', msg, err, ']');
  debug('Error', err.stack);

  results.status = 'failure';
  results.err = err;
  results.message = msg;
  writeResult();

  setTimeout(function() {
    debug('Exit process with errors');
    process.exit(1);
  }, 150); // let write to stdout complete
}

function CheckFilesizeInBytes(filename) {
  var stats = fs.statSync(filename);
  return stats["size"];
}

function callNightWatch(browserEnv) {
  writeSnapshot('callNightWatch');

  var argv = {
    config: confPath,
    env: browserEnv,
    verbose: 1,
    test: scriptPath,
  };

  debug('call Nightwatch.runner', argv);

  var rtcFile = path.join(webRTC_dir, webRTC_fileName);
  var rtcFile_FinalDestination = path.join(resultPath, webRTC_fileName);
  var getstatLogFile = path.join(resultPath, getstatLog_fileName);
  var browserLogFile = path.join(resultPath, browserLog_fileName);
  var performanceFile = path.join(resultPath, performance_fileName);


  // List of screenshot filenames to upload
  var screenshotItems = rtcTestInfo.getScreenshots(),
      screenshotsFilesList = screenshotItems.map(function(item) { return item.filename; });

  var files = [rtcFile, getstatLogFile, browserLogFile];
  files.forEach(function(file) {
    fs.existsSync(file) && fs.unlinkSync(file);
  });

  // Start RTCVideoServer instance just for Firefox
  var rtcVideoServer;
  if (browserEnv === 'firefox' && !(jsonTestOptions.options.disableVideo && jsonTestOptions.options.disableAudio)) {
    var mediaPath;

    if (jsonTestOptions.videoPath && !jsonTestOptions.options.disableVideo) {
      mediaPath = jsonTestOptions.videoPath;
    } else if (jsonTestOptions.audioPath && !jsonTestOptions.options.disableAudio) {
      mediaPath = jsonTestOptions.audioPath;
    }

    if (mediaPath) {
      rtcVideoServer = new RTCVideoServer(mediaPath);
      rtcVideoServer.start(8888);
      debug('RTCVideoServer instance is running on port 8888');
    }
  }

  // Start polling for free memory and cpu usage
  var perf;
  if (jsonTestOptions.options.perf) {
    perf = new RTCPerformanceUtil();
    perf.start();
  }

  runner = Nightwatch.runner(argv, function(result) {
    debug('Nightwatch completed', result);

    if (rtcVideoServer) {
      rtcVideoServer.stop();
      debug('RTCVideoServer has stopped');
    }

    var resetCommands = IPTABLES_RESET_COMMANDS.concat(COMCAST_RESET_COMMANDS);

    executeCmd(null, resetCommands, jsonTestOptions.allowedCommands).then(function() {
      // Done waiting for reset commands

      progress('Upload test files');

      var uploader = new RTCUploader.AzureUploader(process.env.AZURE_STORAGE_CONNECTION_STRING, jsonTestOptions.blobOptions.container);

      files.forEach(function(file) {
        debug('FILE:', file, fs.existsSync(file) ? (CheckFilesizeInBytes(file) + 'byte') : 'not found');
      });

      results.screenshots = []; // List of remote screenshot names
      results.status = result ? 'completed' : 'failure';
      results.rtcTestInfo = rtcTestInfo.get();

      // Collect warnings and other messages added by addMessage()
      collectMessages();

      if (perf) {
        // Stop performance interval and dump date to file
        perf.stop(performanceFile);
      }

      if (fs.existsSync(browserLogFile)) {
        uploader.queue(browserLog_fileName, browserLogFile, {
          azure: {
            folder: jsonTestOptions.blobOptions.folder,
            compress: true,
            getURL: false
          }
        });
      }

      if (fs.existsSync(getstatLogFile)) {
        uploader.queue(getstatLog_fileName, getstatLogFile, {
          azure: {
            folder: jsonTestOptions.blobOptions.folder,
            compress: true,
            getURL: false
          }
        });
      }

      if (fs.existsSync(rtcFile)) {
        uploader.queue(webRTC_fileName, rtcFile, {
          azure: {
            folder: jsonTestOptions.blobOptions.folder,
            compress: true,
            getURL: false
          }
        });
      }

      if (fs.existsSync(performanceFile)) {
        uploader.queue(performance_fileName, performanceFile, {
          azure: {
            folder: jsonTestOptions.blobOptions.folder,
            compress: true,
            getURL: false
          }
        });
      }

      screenshotItems.forEach(function(screenshotItem) {
        uploader.queue(screenshotItem.filename, path.join(resultPath, screenshotItem.filename), {
          azure: {
            folder: jsonTestOptions.blobOptions.folder,
            compress: false,
            contentType: 'image/png',
            getURL: false,
            metadata: {
              url: screenshotItem.url
            }
          }
        });
      });

      if (process.env.AUDIO_CAPTURE) {
        // Search for captured audio file

        getCapturedAudioFile(function(err, file) {
          if (err) {
            addMessage('AUDIO-REC', 'warn', err);
            debug('Error occured while searching for aecdump or during its convertation to WAV', err.stack);
            return doFinalize();
          }

          var fnVoiceQualityAnalysis = function noopAsync(file, cb) { return cb(null); }
          if (jsonTestOptions.options['voice-quality']) {
            progress('Analyzing voice quality');
            fnVoiceQualityAnalysis = runVoiceQualityAnalysis;
          }

          fnVoiceQualityAnalysis(file, function(err, pvqaReport) {
            if (err) {
              debug('Error running voice quality analysis', err.stack);
              // Do not interrupt execution
            }

            if (pvqaReport) {
              // We have a report - upload samples to the cloud
              uploader.queue(pvqaReport_fileName, pvqaReport.samples, {
                azure: {
                  folder: jsonTestOptions.blobOptions.folder,
                  compress: false,
                  getURL: false
                }
              });

              rtcTestInfo.addValue('metric', 'VoiceQuality:MOS', {op: 'set', val: pvqaReport.metrics.MOS, agg: 'avg'});
              rtcTestInfo.addValue('metric', 'VoiceQuality:DeadAir', {op: 'set', val: pvqaReport.metrics.DeadAirPct, agg: 'avg'});
              rtcTestInfo.addValue('metric', 'VoiceQuality:Click', {op: 'set', val: pvqaReport.metrics.ClickPct, agg: 'avg'});
            }

            if (jsonTestOptions.options.mediaStorage) {
              progress('Uploading audio recording');

              var audioUploader;
              if (jsonTestOptions.options.useSFTP) {
                debug(util.format('Going to upload audio file to sftp://%s@%s:%s%s', jsonTestOptions.sftpOptions.username, jsonTestOptions.sftpOptions.host, jsonTestOptions.sftpOptions.port, jsonTestOptions.sftpOptions.path));

                audioUploader = new RTCUploader.SFTPUploader(jsonTestOptions.sftpOptions, {
                  zip: jsonTestOptions.sftpOptions.fileName,
                  folder: typeof jsonTestOptions.options.mediaStorage == 'string' ? jsonTestOptions.options.mediaStorage : null
                });
              } else {
                debug(util.format('Going to upload audio file to Azure: %s:%s', jsonTestOptions.blobOptions.folder, reverseWav_fileName));

                audioUploader = new RTCUploader.AzureUploader(process.env.AZURE_STORAGE_CONNECTION_STRING, jsonTestOptions.blobOptions.container);
              }

              audioUploader.queue(reverseWav_fileName, file, {
                azure: {
                  folder: jsonTestOptions.blobOptions.folder,
                  compress: false,
                  getURL: true
                }
              });

              audioUploader.uploadAsync(function(err, files) {
                if (err) {
                  addMessage('AUDIO-REC', 'warn', util.format('Failed to upload audio recording file (%s)', err));
                  debug('Error uploading audio file', err.stack);
                }

                if (files && files.length > 0) {
                  if (jsonTestOptions.options.useSFTP) {
                    results.audio = files[0].remoteName;
                    debug(util.format('SFTP Audio recording path: %s', results.audio));
                    addMessage('SYSTEM', 'info', util.format('SFTP Audio recording path: %s', results.audio));
                  } else {
                    results.audio = files[0].remoteName;
                    debug(util.format('Audio recording Azure URL:', files[0].url));
                    addMessage('SYSTEM', 'info', util.format('Audio recording Azure URL:', files[0].url));
                  }
                }

                doFinalize();
              });
            } else {
              doFinalize();
            }
          });
          
        });
      } else {
        doFinalize();
      }

      function doFinalize() {
        // Run uploader
        uploader.uploadAsync(function(err, files) {
          if (err) {
            debug('Error uploading files', err);
          }

          debug('Uploaded files', files);
          files.forEach(function(file) {
            if (file.name === browserLog_fileName) {
              results.browserLog = file.remoteName;
            } else if (file.name === getstatLog_fileName) {
              results.getstatLog = file.remoteName;
            } else if (file.name === webRTC_fileName) {
              results.rtcFile = file.remoteName;
            } else if (file.name === performance_fileName) {
              results.performance = file.remoteName;
            } else if (screenshotsFilesList.indexOf(file.name) !== -1) {
              results.screenshots.push(file.remoteName);
            } else if (file.name === pvqaReport_fileName) {
              results.pvqa = file;
            }
          });

          exit();
        });
      }
    }).catch(function(err) {
      debug('Error while running reset commands or finalizing:', err);
      exit();
    });

  function exit() {
    writeSnapshot('end', function (err) {
      writeCPUProfile('cputprofile', function (err) {
        writeResult();
        progress('Upload test files completed');
        debug('commit...');
        setTimeout(function () {
          debug('Exit process after write results');
          process.exit(0);
        }, 150);
      });
    });
  }

  }, {});
}

function executeCmd(script, commandExec, allowedCommands) {
  var deferred = Q.defer();

  if (Array.isArray(allowedCommands)) {
    allowedCommands = ALLOWED_COMMANDS.concat(allowedCommands);
  }

  function logLines(prefix, std) {
    if (std) {
      var lines = std.split('\n');
      for (var i = 0; i < lines.length-1; i++) {
        debug(prefix, lines[i]);
      }
    }
  }

  function executeCmd1(cmd, callback) {
    var whiteList = -1;
    for (var i=0; i<allowedCommands.length; i++) {
      var allowed = allowedCommands[i];
      if (cmd.cmd.indexOf(allowed) === 0 || cmd.cmd.indexOf('sudo ' + allowed) === 0) {
        whiteList = i;
        break;
      }
    }

    if (whiteList >= 0) {
      var command = cmd.cmd;
      if (command.indexOf('comcast') === 0) {
        command = './bin/' + command;
      }
      else if (command.indexOf('sudo comcast') === 0) {
        command = 'sudo ./bin/' + command.substring(5);
      }

      if (command.indexOf('iptables') === 0) {
        command = 'sudo ' + command;
      }

      command = command.replace('iptables ', 'iptables -w ');


      debug(cmd.name, 'execute command', command, cmd.options ? cmd.options : '');
      var child = exec(command, cmd.options, function (err, stdout, stderr) {
        logLines(cmd.name + '-OUT:', stdout);
        logLines(cmd.name + '-ERR:', stderr);
        debug(cmd.name, '--------------------------');

        callback(null);
      });
    } else {
      callback('Command is not allowed: ' + cmd.cmd);
    }
  }

  commandExec = commandExec || [];
  
  if (script) {
    var lines;
    script = script.replace(/\r\n/g, '\n');
    lines = script.toString().split('\n');

    var lineno = 1;
    for (var i = 0; i < lines.length; i++) {
      var line = lines[i];

      var whiteList = -1;
      for (var j = 0; j < allowedCommands.length; j++) {
        var allowed = allowedCommands[j];
        if (line.indexOf('//!' + allowed) === 0 || line.indexOf('//!sudo ' + allowed) === 0) {
          whiteList = j;
        }
      }

      if (whiteList >= 0) {
        debug('Add command', lineno, line.substring(3));
        commandExec.push({name: lineno, cmd: line.substring(3)});
      }

      lineno++;
    }
  }

  try {
    async.eachSeries(commandExec, executeCmd1, function(err) {
      debug('executeCmd completed');
      deferred.resolve();
    });
  } catch(err) {
    async.setImmediate(function() {
      deferred.reject(err);
    });
  }

  return deferred.promise;
}

function waitForSelenium() {
  debug('waitForSelenium', jsonTestOptions.seleniumWait);
  setTimeout(function() {
    if (jsonTestOptions.testScript) {
      try {
        callNightWatch(jsonTestOptions.browserEnv);
      } catch (err) {
        handleError(err, 'Exception during callNightWatch');
      }
    } else {
      handleError('settings', 'Selenium is ready but didnt received test option is stdin');
    }
  }, jsonTestOptions.seleniumWait || 3000);
}

function getCapturedAudioFile(cb) {
  // TODO: will probably need to do directory detection before running Nightwatch
  getExpectedDownloadDirectory(function(err, directory) {
    if (err || !directory) {
      // Unable to detect default download location so we can't continue
      debug('Unable to detect default download location. Error:', err);
      return cb(err);
    }

    debug('AUDIO_CAPTURE: directory', directory);
    fs.readdir(directory, function(err, files) {
      if (err) {
        debug('Unable to read download directory entries. Error:', err);
        return cb(err);
      }

      debug('AUDIO_CAPTURE: files', files);
      // TODO: there could be multiple dumps

      files = files.filter(function(file) {
        return /^audio_debug\.[0-9]+\.aec_dump\.[0-9]+$/.test(file);
      }).map(function(file) {
        return {
          file: file,
          stat: fs.statSync(path.join(directory, file))
        };
      });

      files.sort(function(a, b) {
        return a.stat.mtime.getTime() - b.stat.mtime.getTime();
      });

      if (files.length > 0) {
        debug('Found dumps: ', files);
        var aecDumpFile = path.join(directory, files[files.length - 1].file);
        var aecDumpPath = path.resolve('./bin/unpack_aecdump');
        debug('Audio file: ' + aecDumpFile, 'exec: ' + aecDumpPath);

        if (fs.existsSync(aecDumpFile)) {
          spawnAsync(aecDumpPath, [aecDumpFile], { cwd: directory }, true, function(err, stdout) {
            if (err) {
              return cb(err);
            }

            var reverseFile = path.join(directory, 'reverse.wav');
            if (!fs.existsSync(reverseFile)) {
              return cb(new Error('Can\'t find reverse.wav file'));
            }

            cb(null, reverseFile);
          });
        } else {
          cb(new Error('Failed to find audio recording file, file does not exists'));
        }
      } else {
        cb(new Error('Failed to find audio recording files - no files found'));
      }
    });
  });
}

function runVoiceQualityAnalysis(file, cb) {
  prepareWavFile(file, function(err, file) {
    if (err) {
      // Convertation failed
      return cb(err);
    }

    var pvqaBasePath = '/browsers/pvqa/pvqa';
    var pvqaPath = path.join(pvqaBasePath, 'pvqa'),
        pvqaLicensePath = path.join(pvqaBasePath, 'pvqa.lic'),
        pvqaSettingsPath = path.join(pvqaBasePath, 'Settings.cfg'),
        pvqaReportPath = path.join(resultPath, 'pvqa.csv'),
        args = [pvqaLicensePath, 'analysis', pvqaReportPath, pvqaSettingsPath, file, '0.799'],
        result = {
          metrics: {
            MOS: null,
            DeadAirPct: null,
            ClickPct: null
          },
          samples: []
        };

    debug('Running PVQA analysis on file:', file, 'Args:', args);

    spawnAsync(pvqaPath, args, null, true, function(err, stdout) {
      if (err) {
        return cb(err);
      }

      if (!fs.existsSync(pvqaReportPath)) {
        return cb(new Error('Can\'t find pvqa report file'));
      }

      var csv = fs.readFileSync(pvqaReportPath, 'utf8'),
          lines = csv.split('\n'),
          headers;

      if (!lines.length) {
        return cb(new Error('PVQA report is empty'));
      }

      headers = lines[0].split(';').map(function(cell) { return cell.trim(); });
      headers[8] = 'Ok'; // Add missing header for ok/poor column

      var deadAir01Count = 0,
          click00Count = 0;

      lines.forEach(function (line, idx) {
        if (idx === 0 || !line) {
          // Skip headers line and empty lines
          return;
        }

        var obj = {};
        
        line.split(';').forEach(function(cell, idx) {
          var key = headers[idx];
          if (!key) {
            return;
          }

          cell = cell.trim();
          obj[key] = isNaN(cell) ? cell : parseFloat(cell);
        });

        if (!isNaN(obj['DeadAir-01']) && obj['DeadAir-01'] > 0) {
          deadAir01Count++;
        }
        if (!isNaN(obj['Click-00']) && obj['Click-00'] > 0) {
          click00Count++;
        }

        result.samples.push(obj);
      });

      result.metrics.DeadAirPct = Math.round(deadAir01Count / result.samples.length * 100);
      result.metrics.ClickPct = Math.round(click00Count / result.samples.length * 100);

      // Getting MOS metric from stdout
      // Searching for the line in the following format: 'MOS = 3.729469'
      stdout.split('\n').filter(function(line) { return line.startsWith('MOS'); }).forEach(function(line) {
        var match = line.split(' ')[2];
        if (!isNaN(match)) {
          result.metrics.MOS = parseFloat(match);
        }
      });

      cb(null, result);
    });
  });
}

function prepareWavFile(file, cb) {
  var ffmpegPath = '/video/ffmpeg/ffmpeg',
      outFile = file + '.conv.wav';
      args = ['-y', '-i', file, '-ac', 1, '-ar', 8000, outFile];

  debug('Preparing WAV file before PVQA analysis. Args:', args);

  spawnAsync(ffmpegPath, args, null, false, function(err) {
    if (err) {
      return cb(err);
    }

    cb(null, outFile);
  });
}

function spawnAsync(executable, args, options, captureStdout, cb) {
  var stderr = '',
      stdout = '';

  options = options || {};

  debug('Spawning', executable);

  var proc = spawn(executable, args, options)
    .on('close', function(code) {
      debug(executable, 'returned with ', code);
      if (code !== 0) {
        debug(executable, 'stderr:', stderr);
        if (captureStdout) {
          debug(executable, 'stdout:', stdout);
        }
        cb(new Error('Error running ' + executable));
        return;
      }

      // stdout will be empty string in case captureStdout if false
      cb(null, stdout);
    })
    .on('error', function(err) {
      debug('Error spawning ', executable, ':', err);
      cb(err);
    });

  proc.stderr.on('data', function(data) {
    stderr += data;
  });

  if (captureStdout) {
    proc.stdout.on('data', function(data) {
      stdout += data;
    });
  }
}
