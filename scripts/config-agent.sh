#!/usr/bin/env bash

#install new relic monitor
##https://rpm.newrelic.com/accounts/1018977/servers/get_started
##Add the New Relic apt repository
echo deb http://apt.newrelic.com/debian/ newrelic non-free >> /etc/apt/sources.list.d/newrelic.list

wget -O- https://download.newrelic.com/548C16BF.gpg | apt-key add -

apt-get update

##Install the Server Monitor package
apt-get install newrelic-sysmond

##Configure & start the Server Monitor daemon
nrsysmond-config --set license_key=4eed2e76ca82735a0b62f0ea5712935ec1e7c378
/etc/init.d/newrelic-sysmond start

#enable new relic to monitor docker activity
usermod -a -G docker newrelic
