#!/usr/bin/env bash

docker pull registry.testrtc.com:443/mulyoved/chrome-beta:v1.8.4
docker pull registry.testrtc.com:443/mulyoved/chrome-debug-beta:v1.8.4
docker pull registry.testrtc.com:443/mulyoved/chrome-debug-stable:v1.8.4
docker pull registry.testrtc.com:443/mulyoved/chrome-debug-unstable:v1.8.4
docker pull registry.testrtc.com:443/mulyoved/chrome-debug-v42:v1.8.4
docker pull registry.testrtc.com:443/mulyoved/chrome-debug-v43:v1.8.4
docker pull registry.testrtc.com:443/mulyoved/chrome-stable:v1.8.4
docker pull registry.testrtc.com:443/mulyoved/chrome-unstable:v1.8.4
docker pull registry.testrtc.com:443/mulyoved/chrome-v42:v1.8.4
docker pull registry.testrtc.com:443/mulyoved/chrome-v43:v1.8.4
docker pull registry.testrtc.com:443/mulyoved/firefox-debug-stable:v1.8.4
docker pull registry.testrtc.com:443/mulyoved/firefox-stable:v1.8.4
docker pull registry.testrtc.com:443/mulyoved/media-convert:v1.8.4
docker pull registry.testrtc.com:443/mulyoved/ubuntu:14.04
docker pull registry.testrtc.com:443/mulyoved/video-storage:v0.0.4

docker tag registry.testrtc.com:443/mulyoved/chrome-beta:v1.8.4 mulyoved/chrome-beta:v1.8.4
docker tag registry.testrtc.com:443/mulyoved/chrome-debug-beta:v1.8.4 mulyoved/chrome-debug-beta:v1.8.4
docker tag registry.testrtc.com:443/mulyoved/chrome-debug-stable:v1.8.4 mulyoved/chrome-debug-stable:v1.8.4
docker tag registry.testrtc.com:443/mulyoved/chrome-debug-unstable:v1.8.4 mulyoved/chrome-debug-unstable:v1.8.4
docker tag registry.testrtc.com:443/mulyoved/chrome-debug-v42:v1.8.4 mulyoved/chrome-debug-v42:v1.8.4
docker tag registry.testrtc.com:443/mulyoved/chrome-debug-v43:v1.8.4 mulyoved/chrome-debug-v43:v1.8.4
docker tag registry.testrtc.com:443/mulyoved/chrome-stable:v1.8.4 mulyoved/chrome-stable:v1.8.4
docker tag registry.testrtc.com:443/mulyoved/chrome-unstable:v1.8.4 mulyoved/chrome-unstable:v1.8.4
docker tag registry.testrtc.com:443/mulyoved/chrome-v42:v1.8.4 mulyoved/chrome-v42:v1.8.4
docker tag registry.testrtc.com:443/mulyoved/chrome-v43:v1.8.4 mulyoved/chrome-v43:v1.8.4
docker tag registry.testrtc.com:443/mulyoved/firefox-debug-stable:v1.8.4 mulyoved/firefox-debug-stable:v1.8.4
docker tag registry.testrtc.com:443/mulyoved/firefox-stable:v1.8.4 mulyoved/firefox-stable:v1.8.4
docker tag registry.testrtc.com:443/mulyoved/media-convert:v1.8.4 mulyoved/media-convert:v1.8.4
docker tag registry.testrtc.com:443/mulyoved/ubuntu:14.04 mulyoved/ubuntu:14.04
docker tag registry.testrtc.com:443/mulyoved/video-storage:v0.0.4 mulyoved/video-storage:v0.0.4




