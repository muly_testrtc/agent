#! /bin/bash

docker build -f images/StandaloneChrome/Dockerfile -t mulyoved/testrtc-chrome:v0.6.1 .
docker build -f images/NodeChromeDebugHub/Dockerfile -t mulyoved/testrtc-agent:v0.6.1 .
docker build -f images/MediaConvert/Dockerfile -t mulyoved/media-convert:v0.6.1 .

docker build -f images/StandaloneChrome2/Dockerfile -t mulyoved/testrtc-chrome2:v0.6.1 .
docker build -f images/StandaloneDebugChrome2/Dockerfile -t mulyoved/testrtc-debug-chrome2:v0.6.1 .



docker build -f images/NodeChromeDebugHub2/Dockerfile -t mulyoved/testrtc-chrome2-vnc:v0.6.1 .


cd ../docker-selenium
git pull
./bld

cd ../agent
docker build -f images/docker-selenium/Dockerfile -t mulyoved/docker-selenium-agent:v0.6.1 .
docker push
