#Build Image
not needed - docker build -f ./images/graylog/Dockerfile -t mulyoved/graylog:v0.0.1 .

# Create Machine
docker-machine --debug create -d azure \
    --azure-location "East US" \
    --azure-subscription-id="94e4de7a-ea4e-4517-b553-eb9ca650a9b8" \
    --azure-subscription-cert="mycert.pem" misc-services \
    --azure-size "Standard_A1"

azure vm endpoint create misc-services 9001 9001 && \
azure vm endpoint create misc-services -o udp 12201 12201 && \
azure vm endpoint create misc-services 12900 12900 && \
azure vm endpoint create misc-services 12201 12201

#docker-machine
[docker-machine](https://docs.docker.com/machine/install-machine/)

```sh
docker-machine ssh misc-services 'sudo bash' < ./scripts/config-agent.sh && \

cd /e/testrtc/agent
docker-machine -s machine ssh misc-services

sudo su -

#manually edit configuration
vi /etc/default/docker
-g /mnt
delete tls lines

sudo usermod -aG docker ubuntu
service docker restart

curl -L https://github.com/docker/compose/releases/download/1.4.0/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

apt-get update -qqy && \
    apt-get -qqy --no-install-recommends --force-yes install \
    nodejs \
    npm \
    iptables

exit
docker-compose --version 
git clone https://muly_testrtc@bitbucket.org/muly_testrtc/agent.git
cd agent
npm install 

docker-compose stop && /
docker-compose rm -f && /
docker-compose up -d && /
docker-compose logs
```

URL: http://misc-services.cloudapp.net:9000/
bash: docker exec -it 15391a008cd0 ./bin/bash

#graylog slack plugin
download plugin to /graylog/plugin 
see instructions
https://marketplace.graylog.org/addons/2b7c3403-60d8-488e-b4be-79364bde1634

#Graylog searches
Create scatter graph on delta
source:monitor-staging-001 AND level:<9 AND message:selenium answer

and look histogram hour
level:<5 AND facility:monitor-staging-001 AND message:watchdog fired


docker-machine restart misc-services