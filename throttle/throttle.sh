#!/bin/sh

COMCAST_CMD=`which comcast`
DEVICE=eth0
LATENCY=0
BANDWIDTH=0
PACKET_LOSS=0

comcast_stop () {
	$COMCAST_CMD --mode stop
}

comcast_start () {

	comcast_stop

	$COMCAST_CMD \
		--device=$DEVICE \
		--latency=$LATENCY \
		--target-bw=$BANDWIDTH \
		--default-bw=$BANDWIDTH \
		--packet-loss=$PACKET_LOSS% \
		--target-addr=0.0.0.0/0 \
		--target-proto=tcp,udp,icmp 

}

case $1 in

	gprs)
		LATENCY=500
		BANDWIDTH=50
		PACKET_LOSS=2
	;;

	edge)
        LATENCY=300
        BANDWIDTH=250
        PACKET_LOSS=1.5
    ;;

	3g)
        LATENCY=250
        BANDWIDTH=750
        PACKET_LOSS=1.5
    ;;

	dialup)
        LATENCY=185
        BANDWIDTH=40
        PACKET_LOSS=2
    ;;
	
	dsl-poor)
        LATENCY=70
        BANDWIDTH=2000
        PACKET_LOSS=2
    ;;

	dsl-good)
        LATENCY=40
        BANDWIDTH=8000
        PACKET_LOSS=0.5
    ;;

	wifi)
        LATENCY=40
        BANDWIDTH=30000
        PACKET_LOSS=0.2
    ;;

	satellite)
        LATENCY=1500
        BANDWIDTH=50000
        PACKET_LOSS=0.2
    ;;

	*)
		echo "Usage: $0 gprs | edge | 3g | dialup | dsl-poor | dsl-good | wifi | satellite"
		exit 
	;;
esac

comcast_start
echo "[INFO] comcast started."
echo ""



