#To install machine see MISC-SERVICES-INSTALL.md  

azure vm endpoint create misc-services 8083 8083 && \
azure vm endpoint create misc-services 8086 8086 && \
azure vm endpoint create misc-services -o udp 4444 4444 && \
azure vm endpoint create misc-services 3000 3000

```sh
docker-machine ssh misc-services

sudo su -

docker-compose --version 
cd agent
git pull

docker-compose --file grafana.yml stop && /
docker-compose --file grafana.yml rm -f && /
docker-compose --file grafana.yml up -d && /
docker-compose --file grafana.yml logs
```

URL: http://misc-services.cloudapp.net:9000/
bash: docker exec -it 15391a008cd0 ./bin/bash

influxdb connection
http://misc-services.cloudapp.net:8083/

CLI
docker exec -ti agent_InfluxSrv_1 /opt/influxdb/influx

influxdb: user: influxdb pass: influxdb
grafana: admin admin (GRNa3bsRTe5Z)
grafana: muly@testrtc.com

Grfana
Create Data source

CREATE RETENTION POLICY "inf" ON staging DURATION 3650d REPLICATION 1 DEFAULT

#test
curl -X POST -d '[{"name":"foo","columns":["val"],"points":[[23]]}]' 'http://misc-services.cloudapp.net:8086/db/influxdb/series?u=influxdb&p=influxdb'