'use strict';

//Create machine
//docker-machine rm azure-agent-4 && nodejs --harmony ./dli.js machine --create -m azure-agent-4
//docker-machine ssh azure-agent-4 'sudo docker images'

//Delete machine
//docker-machine rm azure-agent-12



var program = require('commander');
var debug = require('debug')('dli');
debug = console.info.bind(console);

var build = require('./docker-admin/docker-build');
var machine = require('./docker-admin/prepare-machine');
var co = require('co');

var cmdValue;
var envValue;

program
  .version('0.0.1')
  .arguments('[cmd]', /^(build|pull|machine|test)$/i)
  //.option('-b, --build', 'Build')
  .option('-p, --push', 'Push')
  .option('-s, --save', 'Save image to local folder instead of push which is slow and fail too much')
  //.option('-q, --pull', 'Pull')
  .option('-d, --dry', 'Dry run')
  .option('-f, --filter [name]', 'Filter image name')
  .option('-m, --machine-name [name]', 'Machine Name')
  .option('-c, --create', 'Create machine')
  .action(function (cmd, env) {
    debug('dli action', cmd);
    cmdValue = cmd;
    envValue = env;
  });

program.parse(process.argv);

debug('dli started', process.argv);

var runMode = {
  push: program.push,
  create: program.create,
  save: program.save,
  dry: program.dry,
  filter: program.filter,
  machineName: program.machineName
};

function *runCmd() {
  var buildProcess;
  var prepareMachine;
  if (!cmdValue) {
    debug('No command specified');
    program.help();
  } else if (cmdValue === 'build') {
    runMode.build = true;
    runMode.pull = false;

    buildProcess = new build.BuildProcess(runMode);
    buildProcess.execute();
  } else if (cmdValue === 'pull') {
    runMode.build = false;
    runMode.pull = true;
    runMode.machineConnectInfo = '';

    buildProcess = new build.BuildProcess(runMode);
    buildProcess.execute();
  } else if (cmdValue === 'machine') {
    prepareMachine = new machine.PrepareMachine(runMode);
    if (runMode.create) {
      yield prepareMachine.create();
    }

    if (runMode.save) {
      yield prepareMachine.loadMachines();
    }
  } else if (cmdValue === 'test') {
    prepareMachine = new machine.PrepareMachine(runMode);
    yield prepareMachine.test();
  } else if (cmdValue === 'cert') {
    var machineName = program.machineName;
    prepareMachine = new machine.PrepareMachine(runMode);
    prepareMachine.parseCert(machineName);
  } else {
    debug('Unknown command', cmdValue);
    program.help();
  }
}

co(runCmd())
  .then(function(answer) {
    debug('runcmd', answer);
  })
  .catch(function(err) {
    debug('error runcmd', err.stack);
  });




