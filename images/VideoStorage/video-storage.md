#how to convert video
ffmpeg -i Mandelbrot.mp4 -ss 10 -t 5 -s 640x480 ma640x480.y4m

#Jenkins
cd video-storage
docker pull registry.testrtc.com:443/mulyoved/video-storage:v0.0.4
docker run --name video -t -i mulyoved/video-storage:v0.0.4
docker cp video:/video/ .

#add image from local pc
scp "/D/DL/nightwatch-webrtc (3)/tests/videos/birds.webm" jenkins@jankins.cloudapp.net:/var/lib/jenkins/video-storage/video
scp "/D/DL/VID_20151216_174217.mp4" build-server-private:/home/muly/video

#copy docker file
cp ../agent/images/VideoStorage/Dockerfile .

docker build -t mulyoved/video-storage:v0.0.4 . && docker push mulyoved/video-storage:v0.0.4