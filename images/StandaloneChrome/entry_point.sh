#!/bin/bash
export GEOMETRY="$SCREEN_WIDTH""x""$SCREEN_HEIGHT""x""$SCREEN_DEPTH"

function shutdown {
  kill -s SIGTERM $NODE_PID
  wait $NODE_PID
}

sudo mkdir $TEST_RESULTS_PATH
sudo chmod +777 $TEST_RESULTS_PATH

echo Run xvfb-run
xvfb-run --server-args="$DISPLAY -screen 0 $GEOMETRY -ac +extension RANDR" \
  java -jar /opt/selenium/selenium-server-standalone.jar &
NODE_PID=$!

echo Run Agent
cd agent
npm run agent &

trap shutdown SIGTERM SIGINT
wait $NODE_PID